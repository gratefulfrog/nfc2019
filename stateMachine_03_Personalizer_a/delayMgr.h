#ifndef DELAY_MGR_H
#define DELAY_MGR_H

#include <Arduino.h>

class DelayMgr{
  protected:
    long del,
         startTime;
    
   public:
    DelayMgr(long ddel);
    void start();
    boolean delayExpired() const;  // return true if time is up
};

#endif
