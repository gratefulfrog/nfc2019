#include "state.h"

// "State 0: Initalization "

const int State::state0nextState = 1;
             
void State::e0(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("entry      action  : 0: Initalization");
    delay(OBS_DELAY);
  #endif
  dMgr->reset();
  dMgr->setupForStepping();
}


void State::d0(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("during     action  : 0 Initalization");
    delay(OBS_DELAY);
  #endif
}

boolean State::t0(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("transition event   : 0 Initalization return true");
    delay(OBS_DELAY);
  #endif
  return true;
}

void State::x0(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("exit       action  : 0 Initalization");
    delay(OBS_DELAY);
  #endif
  if (dMgr->labelDetected()){
    nextStateIndex = 1;
  }
  else{
    nextStateIndex = 2;;
  }
}
