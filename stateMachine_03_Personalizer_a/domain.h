#ifndef DOMAIN_H
#define DOMAIN_H

#include <Arduino.h>
#include "stepper.h"
#include "labelSensor.h"
#include "labelMgr.h"
#include "rpiCommunicator.h"
#include "config.h"

class DomainMgr{
  protected:
    Stepper      *motor      = NULL;
    LabelSensor  *ls         = NULL;
    LabelMgr     *lmgr       = NULL;
    RPICommunicator *rpiC    = NULL;

  public:
    DomainMgr();
    ~DomainMgr();
    void        reset();
    void        showDirection() const;
    void        setForwardDirection (boolean yes);
    void        showLabelCount() const;
    void        showBadLabelString() const;
    void        stepOnce(boolean doCount);
    void        incLabelCountAndUpdateMotorSpeedResetStepCount();
    void        setupForStepping();
    void        setupForStopStepping();

    // labelSensor
    boolean     labelDetected() const;
    boolean     gapDetected() const;
    boolean     gapThenLabelDetected() const;

    // RPiCommunicator
    boolean     ok2AdvanceFlagSet() const;
    boolean     badLabelFlagSet() const;
    void        setOK2Personalize();
    void        setNotOK2Personalize();
    //boolean     pinSetDelayExpired() const;

    // labelMgr
    void markCurrentAsFailed();
};

    //friend class State;
    /*
    // Constants
    static const char forwardChar,
                      backChar,    //= '0',
                      limitChar; //   = 'L';

    /*
    boolean firstLoop = true;
    int     index     = -2,
            lastIndex = -1;
    
    #ifdef DEBUG    // automation for tests without devices
      int   count = 0;
      float limy  = 5;
    #endif
    
    void    _showLabelLimit() const;
    void    _setLabelLimit(int limit);
    void    _showDirection() const;
    void    _showCounts() const;
    void    _showStatus() const;
    void    _startupMessage() const;
    boolean _getUserInput() ;
    void    _stepOnce(int index);
    void    _incShowReset(int newIndex);
    void    _setupForStepping();
    void    _setupForStopStepping();
    void    _doSingleStep();
    boolean _labelCountAttained();
    */



#endif
