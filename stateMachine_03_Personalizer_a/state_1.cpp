#include "state.h"

//"State 1: Wind back to Gap",


const int State::state1nextState = 2;

void State::e1(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("entry      action  : 1 Wind back to Gap setForwardDirection(false)");
    delay(OBS_DELAY);
  #endif
  dMgr->setForwardDirection(false);
}

void State::d1(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("during     action  : 1 Wind back to Gap  stepOnce(false)");
    delay(OBS_DELAY);
  #endif
  dMgr->stepOnce(false);
}

boolean State::t1(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("transition event   : 1 Wind back to Gap gapDetected()");
    delay(OBS_DELAY);
  #endif
  return dMgr->gapDetected();
}

void State::x1(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("exit       action  : 1");
  #endif
}
