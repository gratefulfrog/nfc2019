#include "state.h"

/*
This demos a simple state machine where each state has:
* a single next state
* a single action upon entry
* a single action during
* a single transition event
* a single action on exit

the State motor in the mainLoop is completely general and will never need more code,
All that is needed is to write the entry,during,exit methods, as well as the transiton event tests.

the example states are:   
*  0    state 0, next is 1
*  1    state 1, next is 2
*  2    decision 2, next is 1 if true, 3 if false
*  3    state    3, next is -1,
* -1    end

     s0
      | 
     s1 <------
      |       |
     d2  true ^
      |  false
     s3
      |
 end -1 

 ************************/

DomainMgr *State::dMgr = NULL;

State::State( String name,
              int   nsI,
              vfPtr eA,
              vfPtr dA,
              vfPtr xA,
              bfPtr oE):
  stateName(name),
  nextStateIndex(nsI),
  entryAction(eA),
  duringAction(dA),
  exitAction(xA),
  outEvent(oE){
}


State* State::stateVec[nbStates]; 

const String   State::namesVec[] = {String("State 0: Initalization"), 
                                    String("State 1: Wind back to Gap"),
                                    String("State 2: Wind forward to 1st Label"),
                                    String("State 3: RPi does personalization"),
                                    String("State 4: Advance to next label"),
                                    String("State 5: USER ABORT")};
                                    
const int      State::nsVec[] = {state0nextState,     state1nextState,   state2nextState,    state3nextState,    state4nextState,    state5nextState};
const vfPtr    State::eVec[]  = {&State::e0,          &State::e1,        &State::e2,         &State::e3,         &State::e4,         &State::e5};
const vfPtr    State::dVec[]  = {NULL,                &State::d1,        &State::d2,         NULL,               &State::d4,         NULL      };
const vfPtr    State::xVec[]  = {&State::x0,          NULL,              NULL,               &State::x3,         NULL,               NULL      };
const bfPtr    State::oVec[]  = {&State::t0,          &State::t1,        &State::t2,         &State::t3,         &State::t4,         &State::t5};


void  State::init(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("State initializing");
  #endif
  for (int i=0;i<nbStates;i++){
    if (stateVec[i]){
      delete stateVec[i];
    }
    stateVec[i] =  new State(String(namesVec[i]),nsVec[i],eVec[i],dVec[i],xVec[i],oVec[i]);  
    #ifdef DEBUG_STATE_MACHINE
      Serial.print(String("Instanciated: "));
      stateVec[i]->showName();
    #endif
  }
  if (dMgr){
    dMgr->reset();
  }
  else{
    dMgr = new DomainMgr();
  }
}

void State::showName(){
  Serial.println(this->stateName);
  //Serial.println(nextStateIndex);
}
