#include "state.h"

// "State 3: RPi does personalization", 

const int State::state3nextState = 4;

void State::e3(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("entry      action  : 3");
  #endif
  //delay(500);
  dMgr->setOK2Personalize();  // this starts the wait on pin set delay
  //  delay(500);
}

void State::d3(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("during     action  : 3");
  #endif
}

boolean State::t3(){
  //boolean res =  ((dMgr->pinSetDelayExpired()) &&  (dMgr->ok2AdvanceFlagSet()));
  boolean res =  dMgr->ok2AdvanceFlagSet();
  #ifdef DEBUG_STATE_MACHINE
    static boolean saidIt =  false;
    if (!saidIt){
      Serial.println("transition event   : 3 waiting for RPi to set the ok to advance flag true");
    }
    saidIt = ! res; 
  #endif
  return res; 
}

void State::x3(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("exit       action  : 3");
  #endif
  if (dMgr->badLabelFlagSet()){
    dMgr->markCurrentAsFailed();
  }
}
