#include "state.h"
//"State 2: Wind forward to 1st Label",

const int State::state2nextState = 3; 

void State::e2(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("entry      action  : 2 Wind forward to 1st Label");
    delay(OBS_DELAY);
  #endif
  dMgr->setForwardDirection(true);
}

void State::d2(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("during     action  : 2 Wind forward to 1st Label");
    delay(OBS_DELAY);
  #endif
  dMgr->stepOnce(false);
}

boolean State::t2(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("transition event   : 2 Wind forward to 1st Label");
    delay(OBS_DELAY);
  #endif
  return dMgr->labelDetected();
}

void State::x2(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("exit       action  : 2 Wind forward to 1st Label");
    delay(OBS_DELAY);
  #endif
}
