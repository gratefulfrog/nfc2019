
/* What is going on?
 *  This will try to read a char and a long on the Serial port.
 *  if after serialTimeout ms, not enough bytes are available, it takes what it gets.
 *  I believe that the time out is reinitialized at each byte read, 
 *  i.e. read a byte, wait(timeOut), read a byte, wait(timeOut)...
 *  If there are no bytes after a full timeOUt, it will give up and take what it could.
 *  So that means, if bytes are sent to it in a burst, with nearly no time between each individual byte,
 *  it will ALWAYS read a full burst, or nothing. If it reads nothing, it will read the full burst next
 *  time through the loop.
 */

long serialTimeout = 1000,
     baudRate      = 115200;

struct __attribute__((packed))  messageStruct{
  char      commandLetter;    // char x (abort), p (personalieze, w, (wind), r (rewind), i (info), r (reset), f (failed)
  uint32_t  numVal;           // 0 – 0xFFFFFFFF
};

void setup() {
  Serial.begin(baudRate);
  Serial.setTimeout(serialTimeout);
  while(!Serial);
  
  messageStruct m ={ '#',65};
  Serial.print("\nTest Ouput: ");
  Serial.write((byte*)&m, sizeof(m));
  Serial.println("\nStarting!");
}

void loop() {
  size_t nbRead = 0;
  messageStruct z = {'|',0};
  
  Serial.print(String("Reading ") + String(sizeof(z)) + String(" bytes... "));
  nbRead = Serial.readBytes((byte*)&z, sizeof(z));

  switch(nbRead){
    case 0:
      Serial.println(String("Failed...at ") + String(millis()/1000) + String(" secs elapsed..."));
      break;
    case 1:
    case 2:
    case 3:
    case 4:
      Serial.println(String("Nb Read: ")+String(nbRead) + String(" : Not enough data, rejecting at ") + String(millis()/1000) + String(" secs elapsed..."));;
      break;
    default:  // here we read at least one and at most sizeof(messageStruct) bytes
      Serial.print(String("Nb Read: ")+String(nbRead) + String(" : "));
      Serial.write((byte*)&z, sizeof(z));
      Serial.println();
      z.numVal=0xFFFFFFFF - z.numVal; // just to do something with the numerical value to be sure we can!
      Serial.print(z.commandLetter);
      Serial.print(", ");
      Serial.println(z.numVal,HEX);
  }
  z.commandLetter = '\0';
  z.numVal = 0;
}
