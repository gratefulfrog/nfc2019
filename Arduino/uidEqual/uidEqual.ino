
uint8_t uid1[] = { 0, 0, 0, 0, 0, 0, 0 },
        uid2[] = { 0, 0, 0, 0, 0, 0, 1 };

void setup() {
  Serial.begin(115200);
  while(!Serial);
  delay(500);
  Serial.println (String("uid1 == uid1 ? ") + (uidEqual(uid1,uid1) ? "TRUE" : "FALSE"));
  Serial.println (String("uid1 == uid2 ? ") + (uidEqual(uid1,uid2) ? "TRUE" : "FALSE"));

}

void loop() {
  // put your main code here, to run repeatedly:

}

boolean uidEqual(const uint8_t left[], const uint8_t right[]){
  const uint8_t uidLength = 7;
  boolean res = true;
  for (uint8_t i=0; i < uidLength; i++) {
    if (left[i] != right[i]){
      res = false;
      break;
    }
  }
  return res;
}
