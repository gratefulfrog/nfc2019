#include "labelSensor.h"

/*
volatile int LabelSensor::labelSensorState;
boolean LabelSensor::initialized = false;

void LabelSensor::init(){
  labelSensorState = 0;
}
*/
LabelSensor::LabelSensor(int pin): labelSensorPin(pin), labelSensorState(LABEL_DETECTED){
  pinMode(labelSensorPin,INPUT);
}
int LabelSensor::getSensorValue()   const { 
  return digitalRead(labelSensorPin);
}
boolean LabelSensor::labelDetected() const{ 
  return (getSensorValue() == LABEL_DETECTED);
}

void LabelSensor::setLabelSensorState(int state){
  labelSensorState = state;
}
int LabelSensor::getLabelSensorState() const{
  return labelSensorState;
}
void LabelSensor::updateLabelSensorState(){
  setLabelSensorState(getSensorValue());
}
