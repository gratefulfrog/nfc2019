#include "state.h"

// "State 3: ", 

const int State::state3nextState = 4;

void State::e3(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("entry      action  : 3");
  #endif
}

void State::d3(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("during     action  : 3");
  #endif
}

void State::x3(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("exit       action  : 3");
  #endif
}

boolean State::t3(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("transition event   : 3");
  #endif
  return true;
}
