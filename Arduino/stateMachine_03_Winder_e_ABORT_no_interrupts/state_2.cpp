#include "state.h"
//"State 2: ABORT",

const int State::state2nextState = 0; //State::abortState;

void State::e2(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("entry      action  : 2");
  #endif
  Serial.println("\nArborting!\n\n");
}

/*
void State::d2(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("during     action  : 2");
  #endif
}

void State::x2(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("exit       action  : 2");
  #endif
}
*/

boolean State::t2(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("transition event   : 2");
  #endif
  return true;
}
