#ifndef LABEL_SENSOR_H
#define LABEL_SENSOR_H

#include <Arduino.h>
#include "config.h"

class LabelSensor{
  protected:
    //static boolean initialized;
    
    const int labelSensorPin;
    int labelSensorState;
    
   public:
    //void init();
    LabelSensor(int pin);
    int     getSensorValue() const;
    boolean labelDetected() const;
    void    setLabelSensorState(int state);
    int     getLabelSensorState() const;
    void    updateLabelSensorState();
};

#endif
