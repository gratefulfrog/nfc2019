#ifndef CONFIG_H
#define CONFIG_H

#include <Arduino.h>

// remove comments on next line to run automatic tests
#define DEBUG
//#define MOTOR_SPEED_DEBUG
//#define LABEL_DEBUG
//#define DEBUG_STATE_MACHINE

// for Serial monitor interaction
#define SERIAL_BAUD_RATE   (115200)

#ifdef DEBUG
  /////// Define PINs here!
  #define RELAY_PIN          (6)     // SET to  below for forward direction     
  #define MOTOR_PIN          (7)
  #define ENABLE_PIN         (8)
  #define LABEL_SENSOR_PIN   (9) 
  #define DIR_PIN            (10)
  #define ABORT_PIN          (11)     // configured as INPUT_PULLUP, bring LOW to abort
#else
  /////// Define PINs here!
  #define RELAY_PIN          (31)     // SET to RELAY_FORWARD_DIRECTION below for forward direction     
  #define MOTOR_PIN          (33)
  #define ENABLE_PIN         (35)
  #define LABEL_SENSOR_PIN   (47) 
  #define DIR_PIN            (49)
  #define ABORT_PIN            (50)     // configured as INPUT_PULLUP, bring LOW to abort
#endif

/////// label count to stop
#define LABEL_COUNT_TARGET (10)

/////// Motor Speed and Direction settings
#define MOTOR_FORWARD_DIRECTION  (0)                           // set this to 0 if LOW makes the motor go forward!
#define RELAY_FORWARD_DIRECTION  (!MOTOR_FORWARD_DIRECTION)    // set this to 0 if LOW makes the motor go forward!
#define SPEED_DELAY              (20)
#define MOTOR_DELAY              (4)
#define MIN_SPEED_DELAY          (10)
#define MIN_MOTOR_DELAY          (2)


/////// Set this according to the sensor, 
#define LABEL_DETECTED     (0)       // 0 if LOW when label detected, 1 if High

///// State Machine
#define NB_STATES    (3)
#define OBS_DELAY    (500)

#endif
