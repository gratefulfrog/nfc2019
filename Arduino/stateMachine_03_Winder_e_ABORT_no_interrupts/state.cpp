#include "state.h"

/*
This demos a simple state machine where each state has:
* a single next state
* a single action upon entry
* a single action during
* a single transition event
* a single action on exit

the State motor in the mainLoop is completely general and will never need more code,
All that is needed is to write the entry,during,exit methods, as well as the transiton event tests.

the example states are:   
*  0    state 0, next is 1
*  1    state 1, next is 2
*  2    decision 2, next is 1 if true, 3 if false
*  3    state    3, next is -1,
* -1    end

     s0
      | 
     s1 <------
      |       |
     d2  true ^
      |  false
     s3
      |
 end -1 

 ************************/

boolean State::alwaysTrue(){
  return true;
}


DomainMgr *State::dMgr = NULL;

State::State( String name,
              int   nsI,
              vfPtr eA,
              vfPtr dA,
              vfPtr xA,
              bfPtr oE):
  stateName(name),
  nextStateIndex(nsI),
  entryAction(eA),
  duringAction(dA),
  exitAction(xA),
  outEvent(oE){
}


State* State::stateVec[nbStates]; 

const String   State::namesVec[] = {String("State 0: init and get user input"), 
                                    String("State 1: Do steps and show counts and end"),
                                    String("State 2: ABORT !")};
                                    //String("State 3: "),
                                    //String("State 4: "),
                                    //String("State 5: ")};

const int      State::nsVec[] = {state0nextState,     state1nextState,   state2nextState}; //,    state3nextState,    state4nextState,    state5nextState};
const vfPtr    State::eVec[]  = {&State::e0,          &State::e1,        &State::e2,    }; //     &State::e3,         &State::e4,         &State::e5};
const vfPtr    State::dVec[]  = {NULL,                &State::d1,        NULL,          }; //     &State::d3,         &State::d4,         &State::d5};
const vfPtr    State::xVec[]  = {NULL,                &State::x1,        NULL,          }; //     NULL,               NULL,               NULL};
const bfPtr    State::oVec[]  = {&State::t0,          &State::t1,        &State::t2,    }; //     &State::t3,         &State::t4,         &State::t5};


void  State::init(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("State initializing");
  #endif
  for (int i=0;i<nbStates;i++){
    stateVec[i] =  new State(String(namesVec[i]),nsVec[i],eVec[i],dVec[i],xVec[i],oVec[i]);  
    #ifdef DEBUG_STATE_MACHINE
      Serial.print(String("Instanciated: "));
      stateVec[i]->showName();
    #endif
  }
  if (dMgr){
    delete dMgr;
  }
  dMgr = new DomainMgr();
}

void State::showName(){
  Serial.println(this->stateName);
  //Serial.println(nextStateIndex);
}
