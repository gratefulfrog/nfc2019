#include "state.h"

int State::state1nextState = 2;

void State::e1(){
  Serial.println("entry      action  : 1");
}

void State::d1(){
  Serial.println("during     action  : 1");
}

void State::x1(){
  Serial.println("exit       action  : 1");
}


boolean State::t1(){
  Serial.println("transition event   : 1");
  return true;
}
