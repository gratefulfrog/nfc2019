
#include "state.h"
#include "stateMachine.h"

StateMachine *sm;

void startSerial(){
  Serial.begin(115200);
  while(!Serial);
  delay(50);  
}

void setup() {
  startSerial();
  
  // static method to create the vecgtor of all states in the application
  State::initStateVec();
  
  // init the instance of StateMachine
  sm = new StateMachine();
}

void loop() {
  sm->mainLoop();
}
