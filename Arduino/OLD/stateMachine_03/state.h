#ifndef STATE_H
#define STATE_H

#include <Arduino.h>
#include "config.h"

class State;

typedef void    (State::*vfPtr)();
typedef boolean (State::*bfPtr)();


// this can be used to "simplify"(?) calls via derreference member function pointers
//#define CALL_MEMBER_FN(object,ptrToMember)  ((object).*(ptrToMember))

class State{
   friend class StateMachine;
  protected:
    boolean alwaysTrue(){return true;}
    void e0();
    void e1();
    void e3();
    void d0();
    void d1();
    void d3();
    void x0();
    void x1();
    void x3();
    boolean t0();
    boolean t1();
    boolean t3();
    void dec2();
    int dec2Get();

    int nextStateIndex;
    
    static const unsigned int nbStates = NB_STATES;
    static State* state0;
    static int state0nextState,
               state1nextState,
               state2nextState,
               state3nextState;
    static const int   nsVec[]; 
    static const vfPtr  eVec[],
                        dVec[],
                        xVec[];
    static const bfPtr  oVec[];
    public:
    // statics
    static State* stateVec[];
    static State stateVecS[];
    static void initStateVec();

    vfPtr entryAction  = NULL;   // void     (State::*entryAction)()  = NULL;
    vfPtr duringAction = NULL;   //void     (State::*duringAction)() = NULL;
    vfPtr exitAction   = NULL;   //void     (State::*exitAction)()   = NULL;
    bfPtr outEvent     = NULL;   //boolean  (State::*outEvent)()     = NULL;

    State(int   nextStateIndex,
          vfPtr eA,
          vfPtr dA,
          vfPtr xA,
          bfPtr oE);
};

#endif
