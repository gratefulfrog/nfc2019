#include "state.h"

/*
This demos a simple state machine where each state has:
* a single next state
* a single action upon entry
* a single action during
* a single transition event
* a single action on exit

the State motor in the mainLoop is completely general and will never need more code,
All that is needed is to write the entry,during,exit methods, as well as the transiton event tests.

the example states are:   
*  0    state 0, next is 1
*  1    state 1, next is 2
*  2    decision 2, next is 1 if true, 3 if false
*  3    state    3, next is -1,
* -1    end

     s0
      | 
     s1 <------
      |       |
     d2  true ^
      |  false
     s3
      |
 end -1 

 ************************/

State::State( int   nsI,
              vfPtr eA,
              vfPtr dA,
              vfPtr xA,
              bfPtr oE):
  nextStateIndex(nsI),
  entryAction(eA),
  duringAction(dA),
  exitAction(xA),
  outEvent(oE){
}

State* State::stateVec[4]; 

const int   State::nsVec[] = {state0nextState,state1nextState,state2nextState,state3nextState};
const vfPtr State::eVec[] = {&State::e0,&State::e1,&State::dec2,&State::e3};
const vfPtr State::dVec[] = {&State::d0,&State::d1,NULL,&State::d3};
const vfPtr State::xVec[] = {&State::x0,&State::x1,NULL,&State::x3};
const bfPtr State::oVec[] = {&State::t0,&State::t1,&State::alwaysTrue,&State::t3};


void  State::initStateVec(){
 
  for (int i=0;i<nbStates;i++){
    stateVec[i] =  new State(nsVec[i],eVec[i],dVec[i],xVec[i],oVec[i]);  
  }
}
