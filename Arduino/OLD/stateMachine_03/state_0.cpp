#include "state.h"

int State::state0nextState = 1;
             
void State::e0(){
  Serial.println("entry      action  : 0");
}

void State::d0(){
  Serial.println("during     action  : 0");
}

void State::x0(){
  Serial.println("exit       action  : 0");
}

boolean State::t0(){
  Serial.println("transition event   : 0");
  return true;
}
