#include "state.h"

int State::state2nextState = -1;

void State::dec2(){
  int next = dec2Get();
  Serial.println(String("Next state         : ") + String(next));
  nextStateIndex = next;
}

int State::dec2Get(){
  while (true){
    Serial.print("Enter next state (0,1,3,-1) Other int values will be considered an error. non integers are taken as ZERO! Value : "); // for yes and go to state 1, anything else for no and go to state 3: ");
  while (!Serial.available());
    int val = Serial.parseInt();
    switch (val){
      case -1:
      case 0:
      case 1:
      case 3:
        Serial.println(val);
        return val;
      default:
        Serial.println(String(val) + " Come on!");
        break;
    }
  }
  Serial.println(0);
  return 0;
}
