#include "state.h"

int State::state3nextState = -1;

void State::e3(){
  Serial.println("entry      action  : 3");
}

void State::d3(){
  Serial.println("during     action  : 3");
}

void State::x3(){
  Serial.println("exit       action  : 3");
}

boolean State::t3(){
  Serial.println("transition event   : 3");
  return true;
}
