#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include <Arduino.h>
#include "config.h"
#include "state.h"


class StateMachine{
  protected:
    int currentState =  0,
        lastState    = -1;
    
    boolean stateChanged();
    void    execVoidAction(State*, vfPtr);
    boolean execBooleanTest(State*, bfPtr);
    void    updateLastState();
    void    updateCurrentState();
    void    doExit();
    
  public: 
    StateMachine(){}
    void mainLoop();
};
#endif
