#include "state.h"

//"State 1: Do steps and show",


const int State::state1nextState = 0;

void State::e1(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("entry      action  : 1");
  #endif
  dMgr->_setupForStepping();
}

void State::d1(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("during     action  : 1");
  #endif
  dMgr->_doSingleStep();
}

void State::x1(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("exit       action  : 1");
  #endif
  dMgr->_showCounts();
}


boolean State::t1(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("transition event   : 1");
  #endif
  return dMgr->_labelCountAttained();
}
