#include "config.h"
#include "App.h"

#ifdef RPI_DEBUG
  #include "RPI.h"
#endif 

//Constants
const int labelSensorPin   = LABEL_SENSOR_PIN,
          labelCountTarget = LABEL_COUNT_TARGET;

// Pointer to the App instance
App *app;
RPI *rpi;

// ISR
void labelSensorState() {
  app->isr();
}

void initInterrupts(){
  attachInterrupt(digitalPinToInterrupt(labelSensorPin), labelSensorState, CHANGE);  
}

void setup(){
  Serial.begin(SERIAL_BAUD_RATE);
  while(!Serial);
  delayMS(100);
  #ifdef RPI_DEBUG
    rpi = new RPI(RPI_INPUT_PIN,RPI_OUTPUT_PIN);
  #endif
  app = new App(labelSensorPin,labelCountTarget);
  initInterrupts();
}

void loop(){
  #ifdef RPI_DEBUG
  rpi->step();
  #endif 
  app->mainLoop();
}
