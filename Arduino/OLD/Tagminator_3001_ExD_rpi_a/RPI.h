#ifndef RPI_H
#define RPI_H

#include <Arduino.h>
#include "config.h"
#include "Tools.h"

class RPI {
  protected:
    static const unsigned long personalizationTime = RPI_PERS_TIME; //ms

    const int inputPin,
              outputPin;
              
    boolean personalizing = false;
    unsigned long personalisationStartTime;
    
    void _startPersonalizing();
    void _stopPersonalizing();
    void _doPersonalisation();

  public:
    RPI(int _inputPin, int _outputPin);
    void step();
};

#endif
