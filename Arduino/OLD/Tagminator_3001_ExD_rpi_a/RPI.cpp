#include "RPI.h"

void RPI::_startPersonalizing(){
  digitalWrite(inputPin,HIGH); 
  digitalWrite(outputPin,HIGH);
  personalisationStartTime = millis();
  personalizing = true;
}
void RPI::_stopPersonalizing(){
  personalizing = false;
  digitalWrite(outputPin,LOW);
}

void RPI::_doPersonalisation(){
  static int count = 0;
  if(!((count++) % 10)){
    Serial.println("Personalizing...");
  }
  if(millis()-personalisationStartTime > RPI_PERS_TIME){
    _stopPersonalizing();
  }
} 

RPI::RPI(int _inputPin, int _outputPin):
  inputPin(_inputPin), outputPin(_outputPin){

  pinMode(inputPin,OUTPUT);
  pinMode(outputPin,OUTPUT);

  digitalWrite(inputPin,HIGH);
  digitalWrite(outputPin,HIGH);
  
}

void RPI::step(){
  //Serial.println(String("RPI Input pin : ") + String(digitalRead(inputPin)));
  if (!digitalRead(inputPin)){
    _startPersonalizing();
  }
  if (personalizing){
    _doPersonalisation();
  }
}
