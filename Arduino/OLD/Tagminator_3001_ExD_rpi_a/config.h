#ifndef CONFIG_H
#define CONFIG_H

#include <Arduino.h>

// remove comments on next line to run automatic tests
#define DEBUG
#define MOTOR_SPEED_DEBUG
#define RPI_DEBUG

// for Serial monitor interaction
#define SERIAL_BAUD_RATE   (115200)

/////// Define PINs here!
#define RELAY_PIN          (31)     // SET to FORWARD_DIRECTION below for forward direction     
#define MOTOR_PIN          (33)
#define ENABLE_PIN         (35)
#define LABEL_SENSOR_PIN   (47) 
#define DIR_PIN            (49)

/////// label count to stop
#define LABEL_COUNT_TARGET (10)

/////// Motor Speed and Direction settings
#define FORWARD_DIRECTION  (0)       // set this to 0 if LOW makes the motor go forward!
#define SPEED_DELAY        (50)
#define MOTOR_DELAY        (4)
#define MIN_SPEED_DELAY    (25)
#define MIN_MOTOR_DELAY    (2)


/////// Set this according to the sensor, 
#define LABEL_DETECTED     (0)       // 0 if LOW when label detected, 1 if High


/////// RPI Stuff /////            
#define RPI_INPUT_PIN      (9)   // Arduino sets this pin LOW ot indicate that the RPI can commence personalisation, config as INPUT_PULLUP on RPI
#define RPI_OUTPUT_PIN     (10)  // RPI     sets this pin LOW to indicate it has finished personalisation, config as INPUT_PULLUP on arduino

//// RPI Simulation ///
#define RPI_PERS_TIME      (100)  // millisecs
#endif
