#ifndef APP_H
#define APP_H

#include <Arduino.h>
#include "Stepper.h"
#include "config.h"
#include "Tools.h"

class App{
  protected:
    // Constants
    const int labelSensorPin    = LABEL_SENSOR_PIN,
              dirPin            = DIR_PIN,
              relayPin          = RELAY_PIN,          
              motorPin          = MOTOR_PIN, 
              enablePin         = ENABLE_PIN,
              labelIndex        = LABEL_DETECTED,
              gapIndex          = !labelIndex,
              forwardDirection  = FORWARD_DIRECTION;

    const unsigned int  speedDelay       = SPEED_DELAY,
                        motorDelay       = MOTOR_DELAY;
                        
    int        labelCountTarget = LABEL_COUNT_TARGET;
    
    static const char forwardChar = '1',
                      backChar    = '0',
                      limitChar   = 'L';
              
    // this is used in the ISR
    volatile int state = 0;

    // these are where the step count is maintained
    unsigned int stepVec[2]      = {0,0},
                 lastStepVec[2]  = {0,0},
                 countVec[2]     = {0,0};
    
    // these are for display
    String countNameVec[2],
           stepNameVec[2];

    // motor driver 
    Stepper *motor;

    void    _showLabelLimit();
    void    _setLabelLimit(int limit);
    void    _showDirection();
    void    _showStatus();
    void    _initNameVecs();
    void    _initCounts();
    void    _initPins();
    void    _startupMessage();
    boolean _okToStart();
    void    _stepOnce(int index);
    void    _incShowReset(int newIndex);
    void    _setupForStepping();
    void    _setupForStopStepping();
    void    _reelRewind();
    
  public:
    App(int _labelSensorPin, unsigned int _labelCountTarget);
    void mainLoop();
    void isr();
};

#endif
