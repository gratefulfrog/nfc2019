#include "domain.h"

const char DomainMgr::forwardChar = '1',
           DomainMgr::backChar    = '0',
           DomainMgr::limitChar   = 'L';


Stepper      *DomainMgr::motor      = NULL;
LabelSensor  *DomainMgr::ls         = NULL;
LabelMgr     *DomainMgr::lmgr       = NULL;
 
boolean DomainMgr::initialized = false;

void DomainMgr::reset(){
  if (motor){
    delete motor;
  }
  motor = new Stepper(MOTOR_PIN,
                      ENABLE_PIN,
                      DIR_PIN,
                      RELAY_PIN,
                      MOTOR_FORWARD_DIRECTION,
                      RELAY_FORWARD_DIRECTION,
                      SPEED_DELAY,
                      MOTOR_DELAY);
  if (ls){
    delete ls;
  }
  ls   = new LabelSensor(LABEL_SENSOR_PIN);
  int lastLim = -1;
  if (lmgr){
    lastLim = lmgr->getLabelLimit();
    delete lmgr;
    #ifdef DEBUG_STATE_MACHINE
      Serial.println(String("last lim ;: ") + String(lastLim));
    #endif
  }
  lmgr = new LabelMgr();
  if (lastLim != -1){
    lmgr->setLabelLimit(lastLim);
  }
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("DomainMgr reset!");
  #endif
}

DomainMgr::DomainMgr(){
  reset();
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("DomainMgr instance created!");
  #endif
}

void    DomainMgr::_showLabelLimit() const{
  lmgr->showLabelLimit();
}
void    DomainMgr::_setLabelLimit(int limit){
  lmgr->setLabelLimit(limit);
}

void DomainMgr::_showDirection() const{
  Serial.println(String("Direction   : ") + String(motor->getDirection() == MOTOR_FORWARD_DIRECTION ? "FORWARD" :  "REVERSE"));  
}
void    DomainMgr::_showCounts() const{
  lmgr->showCounts();
}

void DomainMgr::_showStatus() const{
  _showLabelLimit();
  _showDirection();
  _showCounts();
  delayMS(100);
}

void    DomainMgr::_startupMessage()  const{
  _showLabelLimit();
  Serial.print(String("Press :\n* ") + 
               String(forwardChar) +
               String(" : to wind forward\n* ") +
               String(backChar) +
               String(" : to rewind\n* ") +
               String(limitChar) +
               String(" to set label count limit\nEnter a value  : "));
}

boolean DomainMgr::_getUserInput() {
  // and return true if ok to start
  static boolean firstTime=true;
  boolean res = false;
  if (firstTime){
    _startupMessage();
    firstTime=false;
  }
  while(!Serial.available());
  if((Serial.available() > 0)){
    char input= char(Serial.read());
    Serial.println(input);
    firstTime=true;              // set this to get the startup message for next round
    switch(input){
      case forwardChar:
        motor->setForward(true);
        res = true;
        break;
      case backChar:
        motor->setForward(false);
        res = true;
        break;
       case limitChar:
         Serial.print("Enter limit : ");
         while(!Serial.available());
         int lim = Serial.parseInt();
         Serial.println(lim);
         _setLabelLimit(lim);
         break;
    }
  }
  if (res){
    _showStatus();
    delayMS(750);
  }
  return res;
}
void DomainMgr::_stepOnce(int index){
  motor->step();
  lmgr->incIndex(index);
}

void  DomainMgr::_incShowReset(int newIndex){
  lmgr->updateCounts(newIndex);
  motor->updateSpeed(lmgr->getSpeedMultiplier(newIndex));
  lmgr->finishCountUpdate(newIndex);
}

void  DomainMgr::_setupForStepping(){
  motor->enable(true);
  Serial.println("Stepping is initialized!");
}

void DomainMgr::_setupForStopStepping(){
  motor->enable(false);
  Serial.println("\n\n**** Done! ****\n\n");
}

void DomainMgr::_doSingleStep(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("Doing single step!");
  #endif
  #ifdef DEBUG
    LabelSensor::init();
    count++;
    if (count>=limy){
      limy+= 0.003;
      LabelSensor::labelSensorState = !LabelSensor::labelSensorState;
      count=0;
    }
  #else
    isr();  
  #endif
  index = LabelSensor::labelSensorState; // low = label; high = gap
  boolean newIndex = (index != lastIndex);
  lastIndex = index;
  if (!firstLoop && newIndex){ //we are at the end of either a label or a gap
    _incShowReset(index);
  }
  firstLoop = false;
  _stepOnce(index);  
}

boolean DomainMgr::_labelCountAttained(){
  return lmgr->labelCountAttained();
}

void DomainMgr::isr() {
  LabelSensor::labelSensorState = ls->getSensorValue();  // low means label, high means gap!
}
