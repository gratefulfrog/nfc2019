#include "state.h"

//"State 4: ",


const int State::state4nextState = 5;

void State::e4(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("entry      action  : 4");
  #endif
}

void State::d4(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("during     action  : 4");
  #endif
}

void State::x4(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("exit       action  : 4");
  #endif
}

boolean State::t4(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("transition event   : 4");
  #endif
  return true;
}
