  #include "stateMachine.h"

/*
This demos a simple state machine where each state has:
* a single next state
* a single action upon entry
* a single action during
* a single transition event
* a single action on exit

the State motor in the mainLoop is completely general and will never need more code,
All that is needed is to write the entry,during,exit methods, as well as the transiton event tests.

 ************************/

StateMachine::StateMachine():abortPin(ABORT_PIN){
  initAbort();
  State::init();
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("new state machine!");
  #endif
}

void StateMachine::mainLoop(){ 
  if (abortRequested() || (currentState == endState)){             // here we are done
    doExit();
  }
  State *currentStatePtr = State::stateVec[currentState];          // to simplify the pointers
  if (stateChanged()){                                             // Here the state has changed
    updateLastState();
    execVoidAction(currentStatePtr,currentStatePtr->entryAction);  // execute the current state's entry action
  }
  execVoidAction(currentStatePtr,currentStatePtr->duringAction);   // always execute the current state's during action
  if (execBooleanTest(currentStatePtr,currentStatePtr->outEvent)){ // if the current state's out event has occured
    #ifdef DEBUG_STATE_MACHINE
      Serial.println(String("Exiting    state   : ") +               //move to next state
                     String(currentState));
    #endif
    execVoidAction(currentStatePtr,currentStatePtr->exitAction);   // execute the current state's exit action
    updateCurrentState();                                          // update the current state index
  }
}

boolean StateMachine::stateChanged(){
  return currentState != lastState;
}

void    StateMachine::execVoidAction(State* s, vfPtr fptr){
  if (fptr){
    (s->*(fptr))();
    #ifdef DEBUG_STATE_MACHINE
      delay(OBS_DELAY);
    #endif
  }
}

boolean StateMachine::execBooleanTest(State* s, bfPtr fptr){
  return (s->*(fptr))();
}

void StateMachine::updateLastState(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("\nState changed!");
  #endif
  //Serial.println(String("Current state      : ") + String(currentState));
  //Serial.println(String("Last state         : ") + String(lastState));
  
  // update lastState
  lastState=currentState;
}

void StateMachine::updateCurrentState(){
  currentState = State::stateVec[currentState]->nextStateIndex;
}

void StateMachine::doExit(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println(String("\nCurrent state      : ") + String(currentState));
  #endif
  Serial.println("Exiting!");
  while(1);
}

void StateMachine::initAbort(){
  pinMode(abortPin, INPUT_PULLUP);
}

boolean StateMachine::abortRequested(){
  return !digitalRead(abortPin);
}
