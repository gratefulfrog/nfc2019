#include "labelSensor.h"

volatile int LabelSensor::labelSensorState;
boolean LabelSensor::initialized = false;

void LabelSensor::init(){
  labelSensorState = 0;
}
LabelSensor::LabelSensor(int pin): labelSensorPin(pin){ 
  if (!initialized){
    init();
    initialized =true;
  }
  pinMode(labelSensorPin,INPUT);
}
int LabelSensor::getSensorValue()   const { 
  return digitalRead(labelSensorPin);
}
boolean LabelSensor::labelDetected() const{ 
  return (getSensorValue() == LABEL_DETECTED);
}
