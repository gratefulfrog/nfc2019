
#include "stateMachine.h"

StateMachine *sm;

void startSerial(){
  Serial.begin(115200);
  while(!Serial);
  delayMS(50);  
  Serial.println("Started!");
}

/*  UNUSED
// ISR
void labelSensorState() {
  DomainMgr::isr();
}
void initInterrupts(){
  attachInterrupt(digitalPinToInterrupt(LABEL_SENSOR_PIN), labelSensorState, CHANGE);  
}
*/

void setup() {
  startSerial();
  sm = new StateMachine();
  //initInterrupts();   // unused now
}

int count =0;
void loop() {
  sm->mainLoop();
}
