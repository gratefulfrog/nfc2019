#ifndef STATE_H
#define STATE_H

#include <Arduino.h>
#include "config.h"
#include "domain.h"

class DomainMgr;
class State;
typedef void    (State::*vfPtr)();
typedef boolean (State::*bfPtr)();


// this can be used to "simplify"(?) calls via derreference member function pointers
//#define CALL_MEMBER_FN(object,ptrToMember)  ((object).*(ptrToMember))

class State { 
  protected:
    void showName();
    boolean alwaysTrue();
    void    e0(); void e1();    void e2();    void e3();    void e4();    void e5();    void e6();
    void    d0(); void d1();    void d2();    void d3();    void d4();    void d5();    void d6();
    void    x0(); void x1();    void x2();    void x3();    void x4();    void x5();    void x6();
    boolean t0(); boolean t1(); boolean t2(); boolean t3(); boolean t4(); boolean t5(); boolean t6();

    
    
    // statics
    static const unsigned int nbStates = NB_STATES;
    static const int state0nextState,
                     state1nextState,
                     state2nextState,
                     state3nextState,
                     state4nextState,
                     state5nextState,
                     state6nextState;
               
    static const String    namesVec[];
    static const int       nsVec[]; 
    static const vfPtr     eVec[],
                           dVec[],
                           xVec[];
    static const bfPtr     oVec[];
    
    static DomainMgr *dMgr;
    //static LabelMgr  *lmgr;

    public:
    // statics
    static State* stateVec[];
    static void init();

    const String stateName;
    int nextStateIndex;
    
    vfPtr entryAction  = NULL;   // void    (State::*entryAction)()  = NULL;
    vfPtr duringAction = NULL;   //void     (State::*duringAction)() = NULL;
    vfPtr exitAction   = NULL;   //void     (State::*exitAction)()   = NULL;
    bfPtr outEvent     = NULL;   //boolean  (State::*outEvent)()     = NULL;

    State(String name,
          int   nextStateIndex,
          vfPtr eA,
          vfPtr dA,
          vfPtr xA,
          bfPtr oE);
};

#endif
