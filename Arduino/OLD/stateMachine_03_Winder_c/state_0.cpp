#include "state.h"

// "State 0: Init and get user input "


const int State::state0nextState = 1;
             
void State::e0(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("entry      action  : 0: reset the dMgr");
  #endif
  dMgr->reset();
}

void State::d0(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("during     action  : 0");
  #endif

  boolean res  = dMgr->_getUserInput();
  nextStateIndex = res ? 1 : 0;
  
  #ifdef DEBUG_STATE_MACHINE
    Serial.println(String(" res was: ")+ String(res));
  #endif
}
/*
void State::x0(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("exit       action  : 0");
  #endif
}
*/
boolean State::t0(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("transition event   : 0");
  #endif
  return true;
}
