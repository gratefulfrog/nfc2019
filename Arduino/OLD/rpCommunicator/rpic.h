#ifndef RPIC_H
#define RPIC_H

#include <Arduino.h>
#include "config.h"
#include "delayMgr.h"


extern const int  waitingForRPI,
                  readyToAdvance,
                  nowAdvancing;
    


class RPICommunicator{
  protected:
    const int  inPin  = IN_PIN;
    const int  outPin = OUT_PIN;

    const long pTime =  MOVE_TIME;
    
    const int endOfAdvance     = READY_SIGNAL,
              ready2Advance    = READY_SIGNAL;

    const String  sWaitingForRPI    = "Waiting for RPi to finish personalizing...",
                  sRPIReadyForTagy  = "RPi is Ready for Tagy to Advance",
                  sAdvancing        = "Advancing...",
                  sAdvanceEnded     = "Advancing ended!"; 

    const int nbMesg  = 4;
    
    DelayMgr *delMgr  = NULL;

    boolean advancing      = false;

    String mVec[4] = {sWaitingForRPI,
                      sRPIReadyForTagy,
                      sAdvancing,
                      sAdvanceEnded} ;
    /*
    const int waitingI    = 0,
              rpiReadyI   = 1,
              advancingI  = 2,
              advanceEndI = 3;

    */
    enum MessageID {waitingI,rpiReadyI,advancingI,advanceEndI};

    void sayIt(int index);
    void writePinAndWait(int pin, int state);
    void startAdvancing();
    void stopAdvancing();
    boolean ok2StartAdvancing();
    boolean ok2StopAdvancing();
    void prependSpaces();

  public:
    RPICommunicator();
    void mainLoop();
};
#endif
