#include "rpic.h"


void RPICommunicator::sayIt(int msgID){
  Serial.println(mVec[msgID]);   
  delay(WAIT_AFTER_WRITE);
}

void RPICommunicator::writePinAndWait(int pin, int state){
  digitalWrite(pin,state);
  delay(WAIT_AFTER_WRITE);
}

void RPICommunicator::startAdvancing(){
  advancing = true;
  writePinAndWait(outPin,advancing);
  sayIt(advancingI);
  delMgr->start();
}

void RPICommunicator::stopAdvancing(){
  writePinAndWait(outPin, endOfAdvance);
  sayIt(advanceEndI);
  advancing = false;
  sayIt(waitingI);
}

boolean RPICommunicator::ok2StartAdvancing(){
  return ((digitalRead(inPin) == ready2Advance) && !advancing);
}

boolean RPICommunicator::ok2StopAdvancing(){
  return  (advancing && (delMgr->delayExpired()));
}

void RPICommunicator::prependSpaces(){
  for (int i = 0; i< nbMesg ; i++){
    int len = i == 0 ? 0 : mVec[i-1].length();
    for (int j=0;j<len;j++){
      mVec[i] = ' ' + mVec[i];
    }
  }
}

RPICommunicator::RPICommunicator(){
  pinMode(inPin, INPUT);
  pinMode(outPin,OUTPUT);
  Serial.begin(115200);
  while(!Serial);
  delay(100);
  Serial.println("Started!");
  writePinAndWait(outPin,!READY_SIGNAL);
  delMgr = new DelayMgr(pTime);
  prependSpaces();
  sayIt(waitingI);
}
  
void RPICommunicator::mainLoop(){
  if (ok2StartAdvancing()){
    sayIt(rpiReadyI);
    startAdvancing();
  }
  if (ok2StopAdvancing()){
    stopAdvancing();
  }
}
