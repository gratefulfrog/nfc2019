#ifndef CONFIG_H
#define CONFIG_H

#include <Arduino.h>

#define LED_PIN       (LED_BUILTIN)
#define IN_PIN        (11)
#define OUT_PIN       (12)
#define PERS_TIME     (000)
#define MOVE_TIME     (10000)

#define READY_SIGNAL  (LOW)

#define WAIT_AFTER_WRITE    (500)

#endif
