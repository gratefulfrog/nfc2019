
#include "stateMachine.h"


StateMachine *sm;

void setup() {
  Serial.begin(115200);
  while(!Serial);
  delay(50);
  sm = new StateMachine();
}

void loop() {
  sm->mainLoop();
}
