#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include <Arduino.h>

class StateMachine;

struct stateStruct{
  uint8_t  nextStateIndex;
  void     (StateMachine::*entryAction)();
  void     (StateMachine::*duringAction)();
  void     (StateMachine::*exitAction)();
  boolean  (StateMachine::*outEvent)();
};

class StateMachine{
  protected:
    stateStruct stateVec[4];
    int8_t currentState =  0,
           lastState    = -1;
    
    boolean stateChanged();
    void e0();
    void e1();
    void e3();
    void d0();
    void d1();
    void d3();
    void x0();
    void x1();
    void x3();
    boolean t0();
    boolean t1();
    boolean t3();
    boolean alwaysTrue();
    void dec2();
    boolean dec2Test();

  public: 
    StateMachine();
    void mainLoop();
};
#endif
