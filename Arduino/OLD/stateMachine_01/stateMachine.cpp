#include "stateMachine.h"

/*
This demos a simple state machine where each state has:
* a single next state
* a single action upon entry
* a single action during
* a single transition event
* a single action on exit

the State motor in the mainLoop is completely general and will never need more code,
All that is needed is to write the entry,during,exit methods, as well as the transiton event tests.

the example states are:   
*  0    state 0, next is 1
*  1    state 1, next is 2
*  2    decision 2, next is 1 if true, 3 if false
*  3    state    3, next is -1,
* -1    end

     s0
      | 
     s1 <------
      |       |
     d2  true ^
      |  false
     s3
      |
 end -1 

 ************************/


StateMachine::StateMachine(){
  stateStruct s0 = {1,
                   &StateMachine::e0,
                   &StateMachine::d0,
                   &StateMachine::x0,
                   &StateMachine::t0};
  stateStruct s1 = {2,
                   &StateMachine::e1,
                   &StateMachine::d1,
                   &StateMachine::x1,
                   &StateMachine::t1};
  stateStruct d2 = {-1,
                   &StateMachine::dec2,
                   NULL,
                   NULL,
                   &StateMachine::alwaysTrue};
  stateStruct s3 = {-1,
                   &StateMachine::e3,
                   &StateMachine::d3,
                   &StateMachine::x3,
                   &StateMachine::t3};
   stateVec[0] = s0;               
   stateVec[1] = s1;
   stateVec[2] = d2;                 
   stateVec[3] = s3;
}

void StateMachine::mainLoop(){
  if (currentState == -1){
    Serial.println(String("\nCurrent state      : ") + String(currentState));
    Serial.println("End!");
    while(1);
  }
  if (stateChanged()){
    Serial.println("\nState changed!");
    Serial.println(String("Current state      : ") + String(currentState));
    Serial.println(String("Last state         : ") + String(lastState));
    // update lastState
    lastState=currentState;
    // execute the current state's entry action
    if (stateVec[currentState].entryAction){
      (this->*(stateVec[currentState].entryAction))();
      delay(5000);
    }
  }
  // always execute the current state's during action
  if (stateVec[currentState].duringAction){
    (this->*(stateVec[currentState].duringAction))();
    delay(5000);  
  }
  // if the current state's out event has occured
  if ((this->*(stateVec[currentState].outEvent))()){
    //move to next state
    Serial.println(String("Exiting    state   : ") + String(currentState));
    // execute the current state's exit action
    if(stateVec[currentState].exitAction){
      (this->*(stateVec[currentState].exitAction))();
    }
    // update the current state index
    currentState = stateVec[currentState].nextStateIndex;
    delay(5000);
  }
}

boolean StateMachine::stateChanged(){
  return currentState != lastState;
}

void StateMachine::e0(){
  Serial.println("entry      action  : 0");
}
void StateMachine::e1(){
  Serial.println("entry      action  : 1");
}
void StateMachine::e3(){
  Serial.println("entry      action  : 3");
}
void StateMachine::d0(){
  Serial.println("during     action  : 0");
}
void StateMachine::d1(){
  Serial.println("during     action  : 1");
}
void StateMachine::d3(){
  Serial.println("during     action  : 3");
}

void StateMachine::x0(){
  Serial.println("exit       action  : 0");
}
void StateMachine::x1(){
  Serial.println("exit       action  : 1");
}
void StateMachine::x3(){
  Serial.println("exit       action  : 3");
}
boolean StateMachine::t0(){
  Serial.println("transition event   : 0");
  return true;
}
boolean StateMachine::t1(){
  Serial.println("transition event   : 1");
  return true;
}
boolean StateMachine::t3(){
  Serial.println("transition event   : 3");
  return true;
}
boolean StateMachine::alwaysTrue(){
  return true;
}
void StateMachine::dec2(){
  if (dec2Test()){
    Serial.println("Next state         : 1");
    stateVec[2].nextStateIndex = 1;
  }
  else{
    Serial.println("Next state         : 3");
    stateVec[2].nextStateIndex = 3;
  }
}
boolean StateMachine::dec2Test(){
  //if true next stat is 2, else 1
  Serial.print("Enter 1 for yes and go to state 1, anything else for no and go to state 3: ");
  while (!Serial.available());
  char c = Serial.read();
  Serial.println(c);
  return (c=='1');
}
