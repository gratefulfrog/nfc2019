#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include <Arduino.h>
#include "state.h"

#define OBS_DELAY (500)

class StateMachine{
  protected:
    int currentState =  0,
        lastState    = -1;
    
    boolean stateChanged();
    void    execVoidAction(State*, vfPtr);
    boolean execBooleanTest(State*, bfPtr);
    void    updateLastState();
    void    updateCurrentState();
    void    doExit();
    
  public: 
    StateMachine(){}
    void mainLoop();
};
#endif
