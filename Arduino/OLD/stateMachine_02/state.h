#ifndef STATE_H
#define STATE_H

#include <Arduino.h>

class State;

typedef void    (State::*vfPtr)();
typedef boolean (State::*bfPtr)();

#define CALL_MEMBER_FN(object,ptrToMember)  ((object).*(ptrToMember))

class State{
  protected:
    boolean alwaysTrue(){return true;}
    void e0();
    void e1();
    void e3();
    void d0();
    void d1();
    void d3();
    void x0();
    void x1();
    void x3();
    boolean t0();
    boolean t1();
    boolean t3();
    void dec2();
    boolean dec2Test();
    int dec2Get();

  public:
    int      nextStateIndex;
    
    // statics
    static State* stateVec[];
    static void initStateVec();

    vfPtr entryAction  = NULL;   // void     (State::*entryAction)()  = NULL;
    vfPtr duringAction = NULL;   //void     (State::*duringAction)() = NULL;
    vfPtr exitAction   = NULL;   //void     (State::*exitAction)()   = NULL;
    bfPtr outEvent     = NULL;   //boolean  (State::*outEvent)()     = NULL;

    State(int   nextStateIndex,
          vfPtr eA,
          vfPtr dA,
          vfPtr xA,
          bfPtr oE);
};

#endif
