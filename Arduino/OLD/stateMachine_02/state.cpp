#include "state.h"

/*
This demos a simple state machine where each state has:
* a single next state
* a single action upon entry
* a single action during
* a single transition event
* a single action on exit

the State motor in the mainLoop is completely general and will never need more code,
All that is needed is to write the entry,during,exit methods, as well as the transiton event tests.

the example states are:   
*  0    state 0, next is 1
*  1    state 1, next is 2
*  2    decision 2, next is 1 if true, 3 if false
*  3    state    3, next is -1,
* -1    end

     s0
      | 
     s1 <------
      |       |
     d2  true ^
      |  false
     s3
      |
 end -1 

 ************************/

State::State( int   nsI,
              vfPtr eA,
              vfPtr dA,
              vfPtr xA,
              bfPtr oE):
  nextStateIndex(nsI),
  entryAction(eA),
  duringAction(dA),
  exitAction(xA),
  outEvent(oE){
}

 State* State::stateVec[4]; 

void  State::initStateVec(){
  int  nsVec[] = {1,2,-1,-1};
  vfPtr eVec[] = {&State::e0,&State::e1,&State::dec2,&State::e3};
  vfPtr dVec[] = {&State::d0,&State::d1,NULL,&State::d3};
  vfPtr xVec[] = {&State::x0,&State::x1,NULL,&State::x3};
  bfPtr oVec[] = {&State::t0,&State::t1,&State::alwaysTrue,&State::t3};

  for (int i=0;i<4;i++){
    stateVec[i] =  new State(nsVec[i],eVec[i],dVec[i],xVec[i],oVec[i]);  
  }
}
             
void State::e0(){
  Serial.println("entry      action  : 0");
}
void State::e1(){
  Serial.println("entry      action  : 1");
}
void State::e3(){
  Serial.println("entry      action  : 3");
}
void State::d0(){
  Serial.println("during     action  : 0");
}
void State::d1(){
  Serial.println("during     action  : 1");
}
void State::d3(){
  Serial.println("during     action  : 3");
}

void State::x0(){
  Serial.println("exit       action  : 0");
}
void State::x1(){
  Serial.println("exit       action  : 1");
}
void State::x3(){
  Serial.println("exit       action  : 3");
}
boolean State::t0(){
  Serial.println("transition event   : 0");
  return true;
}
boolean State::t1(){
  Serial.println("transition event   : 1");
  return true;
}
boolean State::t3(){
  Serial.println("transition event   : 3");
  return true;
}
void State::dec2(){
  int next = dec2Get();
  Serial.println(String("Next state         : ") + String(next));
  nextStateIndex = next;
}

int State::dec2Get(){
  while (true){
    Serial.print("Enter next state (0,1,3,-1) Other int values will be considered an error. non integers are taken as ZERO! Value : "); // for yes and go to state 1, anything else for no and go to state 3: ");
  while (!Serial.available());
    int val = Serial.parseInt();
    switch (val){
      case -1:
      case 0:
      case 1:
      case 3:
        Serial.println(val);
        return val;
      default:
        Serial.println(String(val) + " Come on!");
        break;
    }
  }
  Serial.println(0);
  return 0;
}
/*
void State::dec2(){
  if (dec2Test()){
    Serial.println("Next state         : 1");
    nextStateIndex = 1;
  }
  else{
    Serial.println("Next state         : 3");
    nextStateIndex = 3;
  }
}
*/
/*
boolean State::dec2Test(){
  //if true next stat is 2, else 1
  Serial.print("Enter 1 for yes and go to state 1, anything else for no and go to state 3: ");
  while (!Serial.available());
  char c = Serial.read();
  Serial.println(c);
  return (c=='1');
}
*/
