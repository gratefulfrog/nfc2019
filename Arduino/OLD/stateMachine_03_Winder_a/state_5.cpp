#include "state.h"

// "State 5: Decision: Label count attained?",

const int State::state5nextState = 0;

void State::e5(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("entry      action  : 5");
  #endif
  dMgr->_showCounts();
}

void State::d5(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("during     action  : 5");
  #endif
}

void State::x5(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("exit       action  : 5");
  #endif
}

boolean State::t5(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("transition event   : 5");
  #endif
  return true;
}
