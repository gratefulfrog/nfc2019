#include "stepper.h"

Stepper::Stepper( int    _motorPin, 
                  int    _enablePin,
                  int    _dirPin,
                  int    _relayPin,
                  int    _forwardDirection,
                  int    _speedDelay,
                  int    _motorDelay): motorPin(_motorPin),
                                       enablePin(_enablePin),
                                       dirPin(_dirPin),
                                       relayPin(_relayPin),
                                       forwardDirection(_forwardDirection),
                                       speedDelay(_speedDelay),
                                       motorDelay(_motorDelay){
  pinMode(motorPin, OUTPUT); 
  pinMode(enablePin, OUTPUT);
  pinMode(dirPin, OUTPUT);   
  pinMode(relayPin, OUTPUT);

  enable(false);
  setForward(true);
} 

void Stepper::enable(boolean yes){
  digitalWrite(enablePin, !yes);
  enabled = yes;
}
 
void Stepper::_setDirection(int dir){
      digitalWrite(dirPin,dir);
      digitalWrite(relayPin,dir);
      currentDirection = dir;
}

void Stepper::setForward(boolean yes){
    _setDirection(yes ? forwardDirection : !forwardDirection);
    #ifdef DEBUG_STATE_MACHINE
      Serial.println(String("setting direction : ") + String(currentDirection));
    #endif
}

int Stepper::getDirection(){
  return currentDirection;
}
void Stepper::step(){
  if (!enabled){
    enable(true);
  }
  digitalWrite(motorPin, HIGH);
  delayMicroseconds(speedDelay);                  
  digitalWrite(motorPin, LOW);
  delayMicroseconds(motorDelay);
}

void Stepper::updateSpeed(float delayFactor){
  motorDelay =  max(minMotorDelay, round(motorDelay  * delayFactor));
  speedDelay =  max(minSpeedDelay, round(speedDelay  * delayFactor));
  #ifdef MOTOR_SPEED_DEBUG
    Serial.println(String("Multiplier : ") + String(delayFactor));
    Serial.print("(motorDelay,speedDelay) update : (");
    Serial.print(motorDelay);
    Serial.print(", ");
    Serial.print(speedDelay);
    Serial.println(")");
  #endif
}
