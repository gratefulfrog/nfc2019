#include "state.h"

//"State 1: User Input",


const int State::state1nextState = 2;

void State::e1(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("entry      action  : 1");
  #endif
}

void State::d1(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("during     action  : 1");
  #endif
  boolean res  = dMgr->_getUserInput();
  nextStateIndex = res ? 2 : 1;
}

void State::x1(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("exit       action  : 1");
  #endif
}

boolean State::t1(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("transition event   : 1");
  #endif
  return true;
}
