#include "state.h"
//"State 2: Decision: Settings?",

const int State::state2nextState = 3;

void State::e2(){
  #ifdef DEBUG_STATE_MACHINE
    showName();
    Serial.println("entry      action  : 2");
  #endif
  dMgr->_setupForStepping();
}

void State::d2(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("during     action  : 2");
  #endif
}

void State::x2(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("exit       action  : 2");
  #endif
}

boolean State::t2(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("transition event   : 2");
  #endif
  return true;
}
