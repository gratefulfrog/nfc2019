#ifndef DOMAIN_H
#define DOMAIN_H

#include <Arduino.h>
#include "stepper.h"
#include "labelSensor.h"
#include "labelMgr.h"
#include "state.h"
#include "config.h"
#include "tools.h"

class DomainMgr{
  friend class State;
  protected:
    // Constants
    static const char forwardChar,
                      backChar,    //= '0',
                      limitChar; //   = 'L';

    static Stepper      *motor;
    static LabelSensor  *ls;
    static LabelMgr     *lmgr;
    static boolean      initialized;

    boolean firstLoop = true;
    int     index     = -2,
            lastIndex = -1;

    #ifdef DEBUG    // automation for tests without devices
      int   count = 0;
      float limy  = 5;
    #endif
  
    void    _showLabelLimit() const;
    void    _setLabelLimit(int limit);
    void    _showDirection() const;
    void    _showCounts() const;
    void    _showStatus() const;
    void    _startupMessage() const;
    boolean _getUserInput() ;
    void    _stepOnce(int index);
    void    _incShowReset(int newIndex);
    void    _setupForStepping();
    void    _setupForStopStepping();
    //void    _reelRewind();
    void    _doSingleStep();
    boolean _labelCountAttained();
    
  public:
    DomainMgr();
    static void reset();
    static void isr();
};

#endif
