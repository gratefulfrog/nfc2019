#ifndef STEPPER_H
#define STEPPER_H

#include <Arduino.h>
#include "config.h"
#include "tools.h"

class Stepper{
  protected:
    static const int minSpeedDelay = MIN_SPEED_DELAY,
                     minMotorDelay = MIN_MOTOR_DELAY;
                     
    const int    motorPin, 
                 enablePin,
                 dirPin,
                 relayPin,
                 forwardDirection;

    int speedDelay,
        motorDelay;

    int currentDirection = forwardDirection;
    boolean enabled = false;
    
    void _setDirection(int dir);    

  public:
    Stepper(int    _motorPin, 
            int    _enablePin,
            int    _dirPin,
            int    _repalyPin,
            int    _forwardDirection,
            int _speedDelay,
            int _motorDelay);

    void enable(boolean yes);   
    void setForward(boolean yes);
    void step();
    int getDirection();
    void updateSpeed(float delayFactor);
};
#endif
