#ifndef APP_H
#define APP_H

#include <Arduino.h>
#include "Stepper.h"
#include "LabelSensor.h"
#include "LabelMgr.h"
#include "config.h"
#include "Tools.h"

class App{
  protected:
    // Constants
    static const char forwardChar = '1',
                      backChar    = '0',
                      limitChar   = 'L';
              
    // this is used in the ISR
    volatile int state = 0;

    // motor driver 
    Stepper *motor    = NULL;
    // Label Sensor and manager
    LabelSensor *ls   = NULL;
    LabelMgr    *lmgr = NULL;

    void    _showLabelLimit() const;
    void    _setLabelLimit(int limit);
    void    _showDirection() const;
    void    _showStatus() const;
    void    _startupMessage() const;
    boolean _getUserInput() ;
    void    _stepOnce(int index);
    void    _incShowReset(int newIndex);
    void    _setupForStepping();
    void    _setupForStopStepping();
    void    _reelRewind();
    
  public:
    App();
    void reset();
    void mainLoop();
    void isr();
};

#endif
