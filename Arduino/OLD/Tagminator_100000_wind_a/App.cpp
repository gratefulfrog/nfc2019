#include "App.h"

void    App::_showLabelLimit() const{
  lmgr->showLabelLimit();
}
void    App::_setLabelLimit(int limit){
  lmgr->setLabelLimit(limit);
}

void App::_showDirection() const{
  Serial.println(String("Direction   : ") + String(motor->getDirection() == FORWARD_DIRECTION ? "FORWARD" :  "REVERSE"));  
}

void App::_showStatus() const{
  _showLabelLimit();
  _showDirection();
  delayMS(100);
}

void    App::_startupMessage()  const{
  _showLabelLimit();
  Serial.print(String("Press :\n* ") + 
               String(forwardChar) +
               String(" : to wind forward\n* ") +
               String(backChar) +
               String(" : to rewind\n* ") +
               String(limitChar) +
               String(" to set label count limit\nEnter a value  : "));
}

boolean App::_getUserInput() {
  // and return tru if ok to start
  static boolean firstTime=true;
  boolean res = false;
  if (firstTime){
    _startupMessage();
    firstTime=false;
  }
  if((Serial.available() > 0)){
    char input= char(Serial.read());
    Serial.println(input);
    firstTime=true;              // set this to get the startup message for next round
    switch(input){
      case forwardChar:
        motor->setForward(true);
        res = true;
        break;
      case backChar:
        motor->setForward(false);
        res = true;
        break;
       case limitChar:
         Serial.print("Enter limit : ");
         while(!Serial.available());
         int lim = Serial.parseInt();
         Serial.println(lim);
         _setLabelLimit(lim);
         break;
    }
  }
  if (res){
    _showStatus();
    delayMS(750);
  }
  return res;
}
void    App::_stepOnce(int index){
  motor->step();
  lmgr->incIndex(index);
}

void  App::_incShowReset(int newIndex){
  lmgr->updateCounts(newIndex);
  motor->updateSpeed(lmgr->getSpeedMultiplier(newIndex));
  lmgr->finishCountUpdate(newIndex);
}

void  App::_setupForStepping(){
  motor->enable(true);
}

void App::_setupForStopStepping(){
  motor->enable(false);
  Serial.println("\n\n**** Done! ****\n\n");
}

void  App::_reelRewind(){
  _setupForStepping();
  boolean firstLoop = true;
  int index     = -2,
      lastIndex = -1;
   
  #ifdef DEBUG    
    int   count = 0;
    float limy  = 5;
    state = 0;
  #endif
  
   while(!lmgr->labelCountAttained()){
      #ifdef DEBUG
      count++;
      if (count>=limy){
        limy+= 0.003;
        state = !state;
        count=0;
      }
    #endif
    
    index = state; // low = label; high = gap
    boolean newIndex = (index != lastIndex);
    lastIndex = index;
    if (!firstLoop && newIndex){ //we are at the end of either a label or a gap
      _incShowReset(index);
    }
    firstLoop = false;
    _stepOnce(index);
  }  
  _setupForStopStepping();
}

void App::reset(){
  if (motor){
    delete motor;
  }
  motor = new Stepper(MOTOR_PIN,
                      ENABLE_PIN,
                      DIR_PIN,
                      RELAY_PIN,
                      FORWARD_DIRECTION,
                      SPEED_DELAY,
                      MOTOR_DELAY);
  if (ls){
    delete ls;
  }
  ls   = new LabelSensor(LABEL_SENSOR_PIN);
  if (lmgr){
    delete lmgr;
  }
  lmgr = new LabelMgr();
}

App::App(){
  reset();
}

void App::mainLoop(){
   if(_getUserInput()){
    lmgr->initCounts();
    _reelRewind();
    reset();
  }
}

void App::isr() {
  state = ls->getSensorValue();  // low means label, high means gap!
}
