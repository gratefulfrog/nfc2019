#include "config.h"
#include "App.h"

// Pointer to the App instance
App *app;

// ISR
void labelSensorState() {
  app->isr();
}

void initSerial(){
  Serial.begin(SERIAL_BAUD_RATE);
  while(!Serial);
  delayMS(100);
}

void initInterrupts(){
  attachInterrupt(digitalPinToInterrupt(LABEL_SENSOR_PIN), labelSensorState, CHANGE);  
}

void setup(){
  initSerial();
  app = new App();
  initInterrupts();
}

void loop(){
  app->mainLoop();
}
