#include "LabelMgr.h"
/*
class LabelMgr{
  protected:
    // these are where the step count is maintained
    unsigned int stepVec[2]      = {0,0},
                 lastStepVec[2]  = {0,0},
                 countVec[2]     = {0,0};

    int          labelCountTarget = LABEL_COUNT_TARGET;
    // these are for display
    String countNameVec[2],
           stepNameVec[2];

    unsigned int labelIndex,
                 gapIndex
*/

void  LabelMgr::initCounts(){
  #ifdef LABEL_DEBUG
    Serial.println("Initializing counts");
  #endif  
  for (int i=0;i<2;i++){
    stepVec[i] = lastStepVec[i] = countVec[i] = 0;   
  }  
}

void LabelMgr::_initNameVecs(){
  countNameVec[labelIndex]   = "Label Count : ";
  countNameVec[gapIndex]     = "Gap   Count : ";
  stepNameVec[labelIndex]    = "Label Steps : ";
  stepNameVec[gapIndex]      = "Gap   Steps : ";
}
LabelMgr::LabelMgr(){
  setLabelLimit(LABEL_COUNT_TARGET);
  initCounts();
  _initNameVecs();
}
boolean LabelMgr::labelCountAttained() const{
  #ifdef LABEL_DEBUG
    Serial.println(String("label count attained ? : ") + String(!(countVec[labelIndex]<labelCountTarget)));
    Serial.println(String("label count : ") + String(countVec[labelIndex]));
    Serial.println(String("label target : ") + String(labelCountTarget)) ;
  #endif
  return (!(countVec[labelIndex]<labelCountTarget));
}
void LabelMgr::setLabelLimit(int lim){
  Serial.println("set label limit");
  labelCountTarget = lim;
}
void LabelMgr::showLabelLimit() const{
  Serial.println(String("Label Limit : ") + String(labelCountTarget));
}
void LabelMgr::updateCounts(int newIndex){
  int vecIndex = !newIndex;
  countVec[vecIndex]++;
  Serial.print(countNameVec[vecIndex]);
  Serial.print(countVec[vecIndex]);
  Serial.print("  -  ");
  Serial.print(stepNameVec[vecIndex]);
  Serial.println (stepVec[vecIndex]);
  if (lastStepVec[vecIndex] == 0){
    lastStepVec[vecIndex] = stepVec[vecIndex];
  }
}

void LabelMgr::finishCountUpdate(int newIndex){
  lastStepVec[newIndex] = stepVec[newIndex];
  stepVec[newIndex] = 0;
}

float LabelMgr::getSpeedMultiplier(int newIndex) const{
  int vecIndex = !newIndex;
  float multiplier = 1.0;
  if((stepVec[vecIndex] != 0) && (countVec[vecIndex]>2)){   // to prevent speed change until we have measured at least one full label
    multiplier = float(lastStepVec[vecIndex])/float(stepVec[vecIndex]);
  }
  return multiplier;
}

void LabelMgr::incIndex(unsigned int index){
  stepVec[index]++;
}
