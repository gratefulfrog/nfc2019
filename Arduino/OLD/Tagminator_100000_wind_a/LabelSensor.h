#ifndef LABEL_SENSOR_H
#define LABEL_SENSOR_H

#include <Arduino.h>
#include "config.h"

class LabelSensor{
  protected:
    const int labelSensorPin;

   public:
    LabelSensor(int pin): labelSensorPin(pin)
                           { pinMode(labelSensorPin,INPUT);}
    int getSensorValue()   { return digitalRead(labelSensorPin);}
    boolean labelDetected(){ return (getSensorValue() == LABEL_DETECTED);}
};

#endif
