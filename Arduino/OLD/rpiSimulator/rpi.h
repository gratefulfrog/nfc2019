#ifndef RPI_H
#define RPI_H

#include <Arduino.h>
#include "config.h"
#include "delayMgr.h"

class RPI{
  protected:
    const int  inPin  = IN_PIN;
    const int  outPin = OUT_PIN;
    
    const long pTime =  PERS_TIME;
    
    const int endOfPers     = READY_SIGNAL,
              ready2Pers    = READY_SIGNAL;


    DelayMgr *delMgr  = NULL;

    long sopTime           = 0;
    boolean saidIt         = false;
    boolean personalizing  = false;

    void sayIt();
    void startPersonalizing();
    void stopPersonalizing();
    boolean ok2StartPersonalizing();
    boolean ok2StopPersonalizing();

  public:
    RPI();
    void mainLoop();
};
#endif
