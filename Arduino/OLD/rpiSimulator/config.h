#ifndef CONFIG_H
#define CONFIG_H


#define IN_PIN        (11)
#define OUT_PIN       (12)
#define PERS_TIME     (2000)

#define READY_SIGNAL  (LOW)

#endif
