#include "rpi.h"

void RPI::sayIt(){
  if (!saidIt){
  saidIt = true;
  boolean r2p       = digitalRead(inPin) == ready2Pers,
          doingPers = personalizing;

  if (doingPers){
    Serial.println("Personalizing...");
  }
  else if (r2p){
      Serial.println("Tagy is Ready for Personalization");  
  }
  else{
    Serial.println("Waiting for Tagy to get ready...");
  }
  saidIt = true;
  delay(500);
  }
}

void RPI::startPersonalizing(){
  delay(1000);
  personalizing = true;
  digitalWrite(outPin,personalizing);
  saidIt= false;
  delMgr->start(); 
}

void RPI::stopPersonalizing(){
  digitalWrite(outPin, endOfPers);
  Serial.println("Personalization ended!");
  personalizing = false;
  saidIt= false;
}

boolean RPI::ok2StartPersonalizing(){
  return (digitalRead(inPin) == ready2Pers && !personalizing);
}

boolean RPI::ok2StopPersonalizing(){
  return  (personalizing && delMgr->delayExpired());
}

RPI::RPI(){
  pinMode(inPin, INPUT_PULLUP);
  pinMode(outPin,OUTPUT);
  Serial.begin(115200);
  while(!Serial);
  delay(100);
  digitalWrite(outPin,READY_SIGNAL);
  delay(500);
  delMgr = new DelayMgr(pTime);
  Serial.println("Started!");
}
  
void RPI::mainLoop(){
  sayIt();
  if (ok2StartPersonalizing()){
    saidIt = false;
    sayIt();
    startPersonalizing();
  }
  if (ok2StopPersonalizing()){
    stopPersonalizing();
  }
}
