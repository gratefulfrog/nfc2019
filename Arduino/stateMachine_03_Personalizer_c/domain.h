#ifndef DOMAIN_H
#define DOMAIN_H

#include <Arduino.h>
#include "stepper.h"
#include "labelSensor.h"
#include "labelMgr.h"
#include "rpiCommunicator.h"
#include "config.h"

class DomainMgr{
  protected:
    Stepper      *motor      = NULL;
    LabelSensor  *ls         = NULL;
    LabelMgr     *lmgr       = NULL;
    RPICommunicator *rpiC    = NULL;

  public:
    DomainMgr();
    ~DomainMgr();
    void        reset();
    void        showDirection() const;
    void        setForwardDirection (boolean yes);
    void        showLabelCount() const;
    void        showBadLabelString() const;
    void        stepOnce(boolean doCount);
    void        incLabelCountAndUpdateMotorSpeedResetStepCount();
    void        setupForStepping();
    void        setupForStopStepping();

    // labelSensor
    boolean     labelDetected() const;
    boolean     gapDetected() const;
    boolean     gapThenLabelDetected() const;

    // RPiCommunicator
    boolean     ok2AdvanceFlagSet() const;
    boolean     badLabelFlagSet() const;
    void        setOK2Personalize();
    void        setNotOK2Personalize();
    //boolean     pinSetDelayExpired() const;

    // labelMgr
    void markCurrentAsFailed();
};

#endif
