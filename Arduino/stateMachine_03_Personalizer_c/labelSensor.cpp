#include "labelSensor.h"


LabelSensor::LabelSensor(int pin): labelSensorPin(pin){ 
  #ifdef DEBUG_PULLUP
    pinMode(labelSensorPin,INPUT_PULLUP);
  #else
    pinMode(labelSensorPin,INPUT);
  #endif
}
int LabelSensor::getSensorValue()   const { 
  return digitalRead(labelSensorPin);
}
boolean LabelSensor::labelDetected() const{ 
  return (getSensorValue() == LABEL_DETECTED);
}

boolean LabelSensor::gapDetected() const{
  return !labelDetected();
}

boolean LabelSensor::gapThenLabelDetected() const{
  static int gapFound = gapDetected(); // init call only

  if (gapFound){
    // if a gap has been found, then we need to wait for a label
    if (labelDetected()){
      gapFound = false; // prepare for next time
      return true;
    }
    // we found a gap but not a label, so fales
    else {
      return false;
    }
  }
  // no gap found, so we're looking for one!
  else {
    gapFound = gapDetected();
    return false;
  }
}
