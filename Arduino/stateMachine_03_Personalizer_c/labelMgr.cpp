#include "labelMgr.h"

LabelMgr::LabelMgr(){
  init();
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("LabelMgr instance creation");
  #endif
}

void  LabelMgr::init(){
  #ifdef LABEL_DEBUG
    Serial.println("Initializing Label Mgr...");
  #endif  
  stepCount = lastStepCount = labelCount    = 0;
  failedLabelStringList = String("");
}

void LabelMgr::incLabelCount(){
  labelCount++;
}
void LabelMgr::incStepCount(){
  stepCount++;
}
void LabelMgr::resetStepCount(){
  lastStepCount = stepCount;
  stepCount = 0;
}

float LabelMgr::getSpeedMultiplier() const{
  float multiplier = 1.0;
  if((stepCount != 0) && (labelCount>2)){   // to prevent speed change until we have measured at least one full label
    multiplier = float(lastStepCount)/float(stepCount);
  }
  return multiplier;
}

void LabelMgr::showLabelCount() const{
    Serial.println(labelCountName + String(labelCount));
}

void LabelMgr::markCurrentAsFailed(){
  failedLabelStringList = failedLabelStringList + String(labelCount) + ",";
}

void LabelMgr::showFailed() const{
  Serial.println(String("Failed label IDs : ") + (failedLabelStringList.length() ? failedLabelStringList.substring(0, failedLabelStringList.length()-1) :  "None"));
}
