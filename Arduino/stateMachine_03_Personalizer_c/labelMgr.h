#ifndef LABEL_MGR_H
#define LABEL_MGR_H

#include <Arduino.h>
#include "config.h"
#include "tools.h"

class LabelMgr{
  protected:              
    unsigned int stepCount     = 0,
                 lastStepCount = 0,
                 labelCount    = 0;

    const String labelCountName =  String("Label Count      : ");

    String failedLabelStringList = String("");   // comma separated list of label indices of labels that faile to personalize
 
  public:
    LabelMgr();
    void    init();
    void    incLabelCount();
    void    incStepCount();
    void    resetStepCount();
    float   getSpeedMultiplier() const;
    void    showLabelCount() const;
    void    markCurrentAsFailed();
    void    showFailed() const;

    
};

#endif
