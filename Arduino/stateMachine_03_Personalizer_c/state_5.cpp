#include "state.h"

// "State 5: RESET"

const int State::state5nextState = 0;

void State::e5(){
  showName();
  Serial.println("\nReset!\n\n");
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("entry      action  : 5 RESET");
    delay(OBS_DELAY);
  #endif
}

void State::d5(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("during     action  : 5 RESET");
    delay(OBS_DELAY);
  #endif
}

boolean State::t5(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("transition event   : 5 RESET return true");
    delay(OBS_DELAY);
  #endif
  return true;
}

void State::x5(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("exit       action  : 5 RESET");
    delay(OBS_DELAY);
  #endif
}
