#include "state.h"

// "State 3: RPi does personalization", 

const int State::state3nextState = 4;

void State::e3(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("entry      action  : 3");
  #endif
  dMgr->setOK2Personalize();  // this starts the wait on pin set delay
  showName();
}

void State::d3(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("during     action  : 3");
  #endif
}

boolean State::t3(){
  boolean res =  dMgr->ok2AdvanceFlagSet();
  static boolean saidIt =  false;
  if (!saidIt){
    Serial.println("Waiting for RPi to set the ok to advance flag true");
  }
  saidIt = ! res; 
  return res; 
}

void State::x3(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("exit       action  : 3");
  #endif  
  if (dMgr->badLabelFlagSet()){
    Serial.println("Bad Label Detected!");
    dMgr->markCurrentAsFailed();
  }
  dMgr->incLabelCountAndUpdateMotorSpeedResetStepCount();
  dMgr->showLabelCount();
  dMgr->showBadLabelString();
}
