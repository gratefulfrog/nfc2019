#include "rpiCommunicator.h"

RPICommunicator::RPICommunicator(){
  #ifdef DEBUG_PULLUP
    pinMode(ok2AdvancePin, INPUT_PULLUP);
    pinMode(badLabelPin, INPUT_PULLUP);
  #else
    pinMode(ok2AdvancePin, INPUT);
    pinMode(badLabelPin, INPUT);
  #endif
  pinMode(ok2PersonalizePin,OUTPUT);
  writePinAndWait(ok2PersonalizePin,!SIGNAL);
}

boolean RPICommunicator::ok2AdvanceFlagSet() const{
  return (digitalRead(ok2AdvancePin) == SIGNAL);
}

boolean RPICommunicator::badLabelFlagSet() const{
  return (digitalRead(badLabelPin) == SIGNAL);
}

void RPICommunicator::setOK2Personalize(){
  writePinAndWait(ok2PersonalizePin,SIGNAL);
}
void RPICommunicator::setNotOK2Personalize(){
  writePinAndWait(ok2PersonalizePin,!SIGNAL);
}

void RPICommunicator::writePinAndWait(int pin, int state){
  digitalWrite(pin,state);
  delay(WAIT_AFTER_WRITE);
}
