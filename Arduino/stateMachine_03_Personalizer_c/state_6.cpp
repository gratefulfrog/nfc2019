#include "state.h"

// "State 6: "

const int State::state6nextState = -1;

void State::e6(){
  showName();
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("entry      action  : 6");
  #endif
}

void State::d6(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("during     action  : 6");
  #endif
}

void State::x6(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("exit       action  : 6");
  #endif
}

boolean State::t6(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("transition event   : 6");
  #endif
  return true;
}
