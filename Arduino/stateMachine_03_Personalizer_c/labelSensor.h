#ifndef LABEL_SENSOR_H
#define LABEL_SENSOR_H

#include <Arduino.h>
#include "config.h"

class LabelSensor{
  protected:
    const int labelSensorPin;
    
   public:
    LabelSensor(int pin);
    int     getSensorValue() const;
    boolean labelDetected() const;
    boolean gapDetected() const;
    boolean gapThenLabelDetected() const;
};

//int labelSensorState;
    

#endif
