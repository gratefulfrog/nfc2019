#include "domain.h"

DomainMgr::DomainMgr(){
  reset();
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("DomainMgr instance created!");
  #endif
}
DomainMgr::~DomainMgr(){
  if (motor){
    delete motor;
  }
  if (ls){
    delete ls;
  }
  if (lmgr){
    delete lmgr;
  }
  if (rpiC){
    delete rpiC;
  }
}

void DomainMgr::reset(){
  if (motor){
    delete motor;
  }
  motor = new Stepper(MOTOR_PIN,
                      ENABLE_PIN,
                      DIR_PIN,
                      RELAY_PIN,
                      MOTOR_FORWARD_DIRECTION,
                      RELAY_FORWARD_DIRECTION,
                      SPEED_DELAY,
                      MOTOR_DELAY);
  if (ls){
    delete ls;
  }
  ls   = new LabelSensor(LABEL_SENSOR_PIN);

  if (lmgr){
    delete lmgr;
  }
  lmgr = new LabelMgr();

  if (rpiC){
    delete rpiC;
  }
  rpiC = new RPICommunicator();
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("DomainMgr reset!");
  #endif
}

void DomainMgr::showDirection() const{
  Serial.println(String("Direction   : ") + String(motor->getDirection() == MOTOR_FORWARD_DIRECTION ? "FORWARD" :  "REVERSE"));  
}
void DomainMgr::setForwardDirection (boolean yes){
  motor->setForward(yes);
}


void    DomainMgr::showLabelCount() const{
  lmgr->showLabelCount();
}

void DomainMgr::showBadLabelString() const{
  lmgr->showFailed();
}


void DomainMgr::stepOnce(boolean doCount){
  motor->step();
  if (doCount){
    lmgr->incStepCount();
  }
}

void DomainMgr::incLabelCountAndUpdateMotorSpeedResetStepCount(){
  lmgr->incLabelCount();
  motor->updateSpeed(lmgr->getSpeedMultiplier());
  lmgr->resetStepCount();
}

void  DomainMgr::setupForStepping(){
  motor->enable(true);
  #ifdef DEBUG
    Serial.println("Stepping is initialized!");
  #endif
}

void DomainMgr::setupForStopStepping(){
  motor->enable(false);
}

boolean DomainMgr::labelDetected() const{
  return ls->labelDetected();
}
boolean DomainMgr::gapDetected() const{
  return ls->gapDetected();
}

boolean DomainMgr::gapThenLabelDetected() const{
  return ls->gapThenLabelDetected();
}

boolean DomainMgr::ok2AdvanceFlagSet() const{
  return (rpiC->ok2AdvanceFlagSet());
}

boolean DomainMgr::badLabelFlagSet() const{
  return rpiC->badLabelFlagSet();
}

void DomainMgr::setOK2Personalize(){
  rpiC->setOK2Personalize();
}

void DomainMgr::setNotOK2Personalize(){
  rpiC->setNotOK2Personalize();
}

void DomainMgr::markCurrentAsFailed(){
  lmgr->markCurrentAsFailed();
}
