#ifndef RPI_COMMUNICATOR_H
#define RPI_COMMUNICATOR_H

#include <Arduino.h>
#include "config.h"

extern const int  waitingForRPI,
                  readyToAdvance,
                  nowAdvancing;

class RPICommunicator{
  protected:
    const int  ok2AdvancePin     = OK_TO_ADVANCE_PIN,
               ok2PersonalizePin = OK_TO_PERSONALIZE_PIN,
               badLabelPin       = BAD_LABEL_PIN;
    
    const long waitAfterSet    = WAIT_AFTER_WRITE;
    
  public:
    RPICommunicator();
   
    boolean ok2AdvanceFlagSet() const;
    boolean badLabelFlagSet() const;
    void    setOK2Personalize();
    void    setNotOK2Personalize();
    //boolean pinSetDelayExpired() const;
    void    writePinAndWait(int pin, int state);

};

#endif
