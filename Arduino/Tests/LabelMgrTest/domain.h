#ifndef DOMAIN_H
#define DOMAIN_H

#include <Arduino.h>
#include "stepper.h"
#include "labelSensor.h"
#include "labelMgr.h"
#include "config.h"
#include "tools.h"

class DomainMgr{
  protected:
    // Constants
    static const char forwardChar = '1',
                      backChar    = '0',
                      limitChar   = 'L';

    static Stepper      *motor;
    static LabelSensor  *ls;
    static LabelMgr     *lmgr;
    static boolean      initialized;

    void    _showLabelLimit() const;
    void    _setLabelLimit(int limit);
    void    _showDirection() const;
    void    _showStatus() const;
    void    _startupMessage() const;
    boolean _getUserInput() ;
    void    _stepOnce(int index);
    void    _incShowReset(int newIndex);
    void    _setupForStepping();
    void    _setupForStopStepping();
    void    _reelRewind();
    
  public:
    //DomainMgr();
    static void reset();
    static void isr();
};

#endif
