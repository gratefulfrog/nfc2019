#include "state.h"

//"State 1: User Input",


const int State::state1nextState = 2;

void State::e1(){
  showName();
  Serial.println("entry      action  : 1");
 // boolean res  = app->_getUserInput();
  //state1nextState = res ? 2 : 1;
}

void State::d1(){
  Serial.println("during     action  : 1");
}

void State::x1(){
  Serial.println("exit       action  : 1");
}

boolean State::t1(){
  Serial.println("transition event   : 1");
  return true;
}
