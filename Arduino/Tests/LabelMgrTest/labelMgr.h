#ifndef LABEL_MGR_H
#define LABEL_MGR_H

#include <Arduino.h>
#include "config.h"
#include "tools.h"

class LabelMgr{
  protected:
    // these are where the step count is maintained
    static const unsigned int labelIndex,
                              gapIndex;
                       
    unsigned int stepVec[2],     // = {0,0},
                 lastStepVec[2], //  = {0,0},
                 countVec[2]; //     = {0,0};

    int          labelCountTarget = LABEL_COUNT_TARGET;
    // these are for display
    String countNameVec[2],
           stepNameVec[2];

    void _initNameVecs();
    
  public:
    LabelMgr();
    void    init();
    boolean labelCountAttained() const;
    void    setLabelLimit(int lim);
    void    showLabelLimit() const;
    void    updateCounts(int newIndex);
    void    finishCountUpdate(int newIndex);
    float   getSpeedMultiplier(int newIndex) const;
    void    incIndex(unsigned int index);
    
};



#endif
