#include "state.h"
//"State 2: Decision: Settings?",

const int State::state2nextState = 3;

void State::e2(){
  showName();
  Serial.println("entry      action  : 2");
}

void State::d2(){
  Serial.println("during     action  : 2");
}

void State::x2(){
  Serial.println("exit       action  : 2");
}

boolean State::t2(){
  Serial.println("transition event   : 2");
  return true;
}
