#ifndef LABEL_SENSOR_H
#define LABEL_SENSOR_H

#include <Arduino.h>
#include "config.h"

class LabelSensor{
  protected:
    static boolean initialized;
    
    const int labelSensorPin;

   public:
    static volatile int labelSensorState;
    static void init();
    LabelSensor(int pin);
    int getSensorValue() const;
    boolean labelDetected() const;
};

#endif
