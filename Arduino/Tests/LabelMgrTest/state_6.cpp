#include "state.h"

// "State 6: End."

const int State::state6nextState = -1;

void State::e6(){
  showName();
  Serial.println("entry      action  : 6");
}

void State::d6(){
  Serial.println("during     action  : 6");
}

void State::x6(){
  Serial.println("exit       action  : 6");
}

boolean State::t6(){
  Serial.println("transition event   : 6");
  return true;
}
