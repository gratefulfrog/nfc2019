#include "state.h"

//"State 4: Step with counting",


const int State::state4nextState = 5;

void State::e4(){
  showName();
  Serial.println("entry      action  : 4");
}

void State::d4(){
  Serial.println("during     action  : 4");
}

void State::x4(){
  Serial.println("exit       action  : 4");
}

boolean State::t4(){
  Serial.println("transition event   : 4");
  return true;
}
