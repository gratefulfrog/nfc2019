#include "state.h"

// "State 0: Started "


const int State::state0nextState = 2;
             
void State::e0(){
  showName();
  Serial.println("entry      action  : 0  : init the DomainMgr");
  //dMgr->reset();
  
}

void State::d0(){
  Serial.println("during     action  : 0");
}

void State::x0(){
  Serial.println("exit       action  : 0");
}

boolean State::t0(){
  Serial.println("transition event   : 0");
  return true;
}
