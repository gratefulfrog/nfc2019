#include "state.h"

// "State 5: Decision: Label count attained?",


const int State::state5nextState = 6;

void State::e5(){
  showName();
  Serial.println("entry      action  : 5");
}

void State::d5(){
  Serial.println("during     action  : 5");
}

void State::x5(){
  Serial.println("exit       action  : 5");
}

boolean State::t5(){
  Serial.println("transition event   : 5");
  return true;
}
