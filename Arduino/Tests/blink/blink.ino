
const int ledPin = LED_BUILTIN;

void setup() {
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin,LOW);
}

void loop() {
  digitalWrite(ledPin, !digitalRead(ledPin));
  delay(500);
}
