#include "test.h"

// Testing the creation of a vector of instances as a static class member

void setup() {
  Serial.begin(115200);
  while (!Serial);
  delay(50);
  Test::init();
  
  for (int i = 0;i<3;i++){
    Serial.println(i);
    Test::tVec[i]->print();
    Test::tVec[i]->add(i).add(i).print();
    Serial.println();
  }
  Serial.println(long(loop));
  Serial.println(long(&loop));
  
}

void loop() {
}
