#ifndef TEST_H
#define TEST_H

#include <Arduino.h>

class Test{
  protected:
    int x,y,z;
  
  public:
    // statics
    static void init();
    static Test* tVec[];

    // constructor
    Test(int xx,int yy, int zz):x(xx),y(yy),z(zz){}

    // methods
    void print() const;
    Test& add(int x);
};

#endif
