#include "test.h"

void Test::print() const{
  Serial.print(String(x) + ", " +String(y) + ", " + String(z) + '\n');
}

Test& Test::add(int a){
  x+=a;
  y+=a;
  z+=a;
  return *this;
}

void Test::init(){
  for (int i=0;i<3;i++){
    Test::tVec[i] = new Test(i,i,i);
  }
}

// definition of the static vector!!
Test* Test::tVec[3];
