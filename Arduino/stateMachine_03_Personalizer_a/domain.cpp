#include "domain.h"

DomainMgr::DomainMgr(){
  reset();
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("DomainMgr instance created!");
  #endif
}
DomainMgr::~DomainMgr(){
  if (motor){
    delete motor;
  }
  if (ls){
    delete ls;
  }
  if (lmgr){
    delete lmgr;
  }
  if (rpiC){
    delete rpiC;
  }
}

void DomainMgr::reset(){
  if (motor){
    delete motor;
  }
  motor = new Stepper(MOTOR_PIN,
                      ENABLE_PIN,
                      DIR_PIN,
                      RELAY_PIN,
                      MOTOR_FORWARD_DIRECTION,
                      RELAY_FORWARD_DIRECTION,
                      SPEED_DELAY,
                      MOTOR_DELAY);
  if (ls){
    delete ls;
  }
  ls   = new LabelSensor(LABEL_SENSOR_PIN);

  if (lmgr){
    delete lmgr;
  }
  lmgr = new LabelMgr();

  if (rpiC){
    delete rpiC;
  }
  rpiC = new RPICommunicator();
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("DomainMgr reset!");
  #endif
}

void DomainMgr::showDirection() const{
  Serial.println(String("Direction   : ") + String(motor->getDirection() == MOTOR_FORWARD_DIRECTION ? "FORWARD" :  "REVERSE"));  
}
void DomainMgr::setForwardDirection (boolean yes){
  motor->setForward(yes);
}


void    DomainMgr::showLabelCount() const{
  lmgr->showLabelCount();
}

void DomainMgr::showBadLabelString() const{
  lmgr->showFailed();
}


void DomainMgr::stepOnce(boolean doCount){
  motor->step();
  if (doCount){
    lmgr->incStepCount();
  }
}

void DomainMgr::incLabelCountAndUpdateMotorSpeedResetStepCount(){
  lmgr->incLabelCount();
  motor->updateSpeed(lmgr->getSpeedMultiplier());
  lmgr->resetStepCount();
}

void  DomainMgr::setupForStepping(){
  motor->enable(true);
  #ifdef DEBUG
    Serial.println("Stepping is initialized!");
  #endif
}

void DomainMgr::setupForStopStepping(){
  motor->enable(false);
  /*
  #ifdef DEBUG
    Serial.println("\n\n**** Done! ****\n\n");
  #endif
  */
}


boolean DomainMgr::labelDetected() const{
  return ls->labelDetected();
}
boolean DomainMgr::gapDetected() const{
  return ls->gapDetected();
}

boolean DomainMgr::gapThenLabelDetected() const{
  return ls->gapThenLabelDetected();
}

boolean DomainMgr::ok2AdvanceFlagSet() const{
  return (rpiC->ok2AdvanceFlagSet());
}

boolean DomainMgr::badLabelFlagSet() const{
  return rpiC->badLabelFlagSet();
}

void DomainMgr::setOK2Personalize(){
  rpiC->setOK2Personalize();
}

void DomainMgr::setNotOK2Personalize(){
  rpiC->setNotOK2Personalize();
}
/*
boolean DomainMgr::pinSetDelayExpired() const{
  return rpiC->pinSetDelayExpired();
}
*/

void DomainMgr::markCurrentAsFailed(){
  lmgr->markCurrentAsFailed();
}

/*
const char DomainMgr::forwardChar = '1',
           DomainMgr::backChar    = '0',
           DomainMgr::limitChar   = 'L';


Stepper      *DomainMgr::motor      = NULL;
LabelSensor  *DomainMgr::ls         = NULL;
LabelMgr     *DomainMgr::lmgr       = NULL;


void  DomainMgr::_incShowReset(int newIndex){
  lmgr->updateCounts(newIndex);
  motor->updateSpeed(lmgr->getSpeedMultiplier(newIndex));
  lmgr->finishCountUpdate(newIndex);
}

void DomainMgr::_doSingleStep(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("Doing single step!");
  #endif
  #ifdef DEBUG
    //ls->setLabelSensorState(LABEL_DETECTED); //LabelSensor::init();
    count++;
    if (count>=limy){
      limy+= 0.003;
      //LabelSensor::labelSensorState = !LabelSensor::labelSensorState;
      ls->setLabelSensorState(!ls->getLabelSensorState());
      count=0;
    }
  #else
    ls->updateLabelSensorState();
  #endif
  //index = LabelSensor::labelSensorState; // low = label; high = gap
  index = ls->getLabelSensorState();  // low = label; high = gap
  boolean newIndex = (index != lastIndex);
  lastIndex = index;
  if (!firstLoop && newIndex){ //we are at the end of either a label or a gap
    _incShowReset(index);
  }
  firstLoop = false;
  _stepOnce(index);  
}

boolean DomainMgr::_labelCountAttained(){
  return lmgr->labelCountAttained();
}
*/
/*
void DomainMgr::isr() {
  LabelSensor::labelSensorState = ls->getSensorValue();  // low means label, high means gap!
}
*/

/*
void DomainMgr::_showStatus() const{
  _showLabelLimit();
  _showDirection();
  _showCounts();
  delayMS(100);
}
*/
/*
void    DomainMgr::_startupMessage()  const{
  _showLabelLimit();
  Serial.print(String("Press :\n* ") + 
               String(forwardChar) +
               String(" : to wind forward\n* ") +
               String(backChar) +
               String(" : to rewind\n* ") +
               String(limitChar) +
               String(" to set label count limit\nEnter a value  : "));
}

boolean DomainMgr::_getUserInput() {
  // and return true if ok to start
  static boolean firstTime=true;
  boolean res = false;
  if (firstTime){
    _startupMessage();
    firstTime=false;
  }
  while(!Serial.available());
  if((Serial.available() > 0)){
    char input= char(Serial.read());
    Serial.println(input);
    firstTime=true;              // set this to get the startup message for next round
    switch(input){
      case forwardChar:
        motor->setForward(true);
        res = true;
        break;
      case backChar:
        motor->setForward(false);
        res = true;
        break;
       case limitChar:
         Serial.print("Enter limit : ");
         while(!Serial.available());
         int lim = Serial.parseInt();
         Serial.println(lim);
         _setLabelLimit(lim);
         break;
    }
  }
  if (res){
    _showStatus();
    delayMS(750);
  }
  return res;
}

void DomainMgr::_stepOnce(int index){
  motor->step();
  lmgr->incIndex(index);
}
*/

/*
void    DomainMgr::_showLabelLimit() const{
  lmgr->showLabelLimit();
}
void    DomainMgr::_setLabelLimit(int limit){
  lmgr->setLabelLimit(limit);
}
*/

/*
void    DomainMgr::_showCounts() const{
  lmgr->showCounts();
}
*/
