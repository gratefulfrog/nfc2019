#include "labelMgr.h"

LabelMgr::LabelMgr(){
  init();
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("LabelMgr instance creation");
  #endif
}

void  LabelMgr::init(){
  #ifdef LABEL_DEBUG
    Serial.println("Initializing Label Mgr...");
  #endif  
  stepCount = lastStepCount = labelCount    = 0;
  failedLabelStringList = String("");
}

void LabelMgr::incLabelCount(){
  labelCount++;
}
void LabelMgr::incStepCount(){
  stepCount++;
}
void LabelMgr::resetStepCount(){
  lastStepCount = stepCount;
  stepCount = 0;
}

float LabelMgr::getSpeedMultiplier() const{
  float multiplier = 1.0;
  if((stepCount != 0) && (labelCount>2)){   // to prevent speed change until we have measured at least one full label
    multiplier = float(lastStepCount)/float(stepCount);
  }
  return multiplier;
}

void LabelMgr::showLabelCount() const{
    Serial.println(labelCountName + String(labelCount));
}

void LabelMgr::markCurrentAsFailed(){
  failedLabelStringList = failedLabelStringList + String(labelCount) + ",";
}

void LabelMgr::showFailed() const{
  Serial.println(String("Failed label IDs : ") + (failedLabelStringList.length() ? failedLabelStringList.substring(0, failedLabelStringList.length()-1) :  "None"));
}

/*
const unsigned int LabelMgr::labelIndex        = LABEL_DETECTED,
                   LabelMgr::gapIndex          = !LABEL_DETECTED;


 void  LabelMgr::init(){
  #ifdef LABEL_DEBUG
    Serial.println("Initializing Label Mgr...");
  #endif  
  for (int i=0;i<2;i++){
    stepVec[i] = lastStepVec[i] = countVec[i] = 0;   
  } 
}

void LabelMgr::_initNameVecs(){
  countNameVec[labelIndex]   = String("Label Count : ");
  countNameVec[gapIndex]     = String("Gap   Count : ");
  stepNameVec[labelIndex]    = String("Label Steps : ");
  stepNameVec[gapIndex]      = String("Gap   Steps : ");
}

LabelMgr::LabelMgr(){
  setLabelLimit(LABEL_COUNT_TARGET);
  init();
  //_initNameVecs();
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("LabelMgr instance creation");
  #endif
}

boolean LabelMgr::labelCountAttained() const{
  #ifdef LABEL_DEBUG
    Serial.println(String("label count attained ? : ") + String(!(countVec[labelIndex]<labelCountTarget)));
    Serial.println(String("label count : ") + String(countVec[labelIndex]));
    Serial.println(String("label target : ") + String(labelCountTarget)) ;
  #endif
  return (!(countVec[labelIndex]<labelCountTarget));
}
void LabelMgr::setLabelLimit(int lim){
  labelCountTarget = lim;
  //Serial.println(String("lable limit set to : ") + String(labelCountTarget));
}
int LabelMgr::getLabelLimit() const{
  return labelCountTarget;
}
void LabelMgr::showLabelLimit() const{
  Serial.println(String("Label Limit : ") + String(labelCountTarget));
}
*/
/*
void LabelMgr::updateCounts(int newIndex){
  int vecIndex = !newIndex;
  countVec[vecIndex]++;
  Serial.print(countNameVec[vecIndex]);
  Serial.print(countVec[vecIndex]);
  Serial.print("  -  ");
  Serial.print(stepNameVec[vecIndex]);
  Serial.println (stepVec[vecIndex]);
  if (lastStepVec[vecIndex] == 0){
    lastStepVec[vecIndex] = stepVec[vecIndex];
  }
}
*/

/*
void LabelMgr::finishCountUpdate(int newIndex){
  lastStepVec[newIndex] = stepVec[newIndex];
  stepVec[newIndex] = 0;
}

float LabelMgr::getSpeedMultiplier(int newIndex) const{
  int vecIndex = !newIndex;
  float multiplier = 1.0;
  if((stepVec[vecIndex] != 0) && (countVec[vecIndex]>2)){   // to prevent speed change until we have measured at least one full label
    multiplier = float(lastStepVec[vecIndex])/float(stepVec[vecIndex]);
  }
  return multiplier;
}

void LabelMgr::incIndex(unsigned int index){
  stepVec[index]++;
  //Serial.println(String("stepVec [") + String(index) +String("] : ") + String(stepVec[index]));
}

 void LabelMgr::showCounts() const{
  for (int i =0;i<2;i++){
    Serial.println(countNameVec[i] + String(countVec[i]));
  }
 }
 */
