
#include "delayMgr.h"


DelayMgr::DelayMgr(long ddel): del(ddel){
}
void DelayMgr::start(){
  startTime = millis();
}
boolean DelayMgr::delayExpired() const{ // return true if
  return millis() - startTime >=del;
}
