#ifndef RPI_COMMUNICATOR_H
#define RPI_COMMUNICATOR_H

#include <Arduino.h>
#include "config.h"
#include "delayMgr.h"


extern const int  waitingForRPI,
                  readyToAdvance,
                  nowAdvancing;

class RPICommunicator{
  protected:
    const int  ok2AdvancePin     = OK_TO_ADVANCE_PIN,
               ok2PersonalizePin = OK_TO_PERSONALIZE_PIN,
               badLabelPin       = BAD_LABEL_PIN;
    
    const long waitAfterSet    = WAIT_AFTER_WRITE;
    
    DelayMgr *delMgr  = NULL;
    
  public:
    RPICommunicator();
   ~RPICommunicator();

    boolean ok2AdvanceFlagSet() const;
    boolean badLabelFlagSet() const;
    void    setOK2Personalize();
    void    setNotOK2Personalize();
    //boolean pinSetDelayExpired() const;
    void    writePinAndWait(int pin, int state);

};

/*
    const int endOfAdvance     = SIGNAL,
              ready2Advance    = SIGNAL;
    */
/*
    const String  sWaitingForRPI    = "Waiting for RPi to finish personalizing...",
                  sRPIReadyForTagy  = "RPi is Ready for Tagy to Advance",
                  sAdvancing        = "Advancing...",
                  sAdvanceEnded     = "Advancing ended!"; 

    const int nbMesg  = 4;
    */
/*
    boolean advancing      = false;

    String mVec[4] = {sWaitingForRPI,
                      sRPIReadyForTagy,
                      sAdvancing,
                      sAdvanceEnded} ;
    enum MessageID {waitingI,rpiReadyI,advancingI,advanceEndI};

    void sayIt(int index);
    void writePinAndWait(int pin, int state);
    void startAdvancing();
    void stopAdvancing();
    boolean ok2StopAdvancing();
    void prependSpaces();
   */
#endif
