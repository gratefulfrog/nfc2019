// based on stateMachine_03_Winder_e_ABORT_no_interrupts


#include "stateMachine.h"

StateMachine *sm;

void startSerial(){
  Serial.begin(115200);
  while(!Serial);
  delayMS(50);  
  Serial.println("Started!");
}

void setup() {
  startSerial();
  sm = new StateMachine();
}

void loop() {
  sm->mainLoop();
}
