#ifndef LABEL_SENSOR_H
#define LABEL_SENSOR_H

#include <Arduino.h>
#include "config.h"

class LabelSensor{
  protected:
    //static boolean initialized;
    
    const int labelSensorPin;
    
   public:
    //void init();
    LabelSensor(int pin);
    int     getSensorValue() const;
    boolean labelDetected() const;
    boolean gapDetected() const;
    boolean gapThenLabelDetected() const;
    /*
    void    setLabelSensorState(int state);
    int     getLabelSensorState() const;
    void    updateLabelSensorState();
    */
};

//int labelSensorState;
    

#endif
