#include "rpiCommunicator.h"

RPICommunicator::RPICommunicator(){
  delMgr = new DelayMgr(waitAfterSet);
  #ifdef DEBUG_PULLUP
    pinMode(ok2AdvancePin, INPUT_PULLUP);
    pinMode(badLabelPin, INPUT_PULLUP);
  #else
    pinMode(ok2AdvancePin, INPUT);
    pinMode(badLabelPin, INPUT);
  #endif
  pinMode(ok2PersonalizePin,OUTPUT);
  writePinAndWait(ok2PersonalizePin,!SIGNAL);
}

RPICommunicator::~RPICommunicator(){
  delete delMgr;
}
boolean RPICommunicator::ok2AdvanceFlagSet() const{
  return (digitalRead(ok2AdvancePin) == SIGNAL);
}

boolean RPICommunicator::badLabelFlagSet() const{
  return (digitalRead(badLabelPin) == SIGNAL);
}

void RPICommunicator::setOK2Personalize(){
  writePinAndWait(ok2PersonalizePin,SIGNAL);
}
void RPICommunicator::setNotOK2Personalize(){
  writePinAndWait(ok2PersonalizePin,!SIGNAL);
}


void RPICommunicator::writePinAndWait(int pin, int state){
  digitalWrite(pin,state);
  delay(WAIT_AFTER_WRITE);
}

/*
boolean RPICommunicator::pinSetDelayExpired() const{
  return delMgr->delayExpired();
}
*/

/*
boolean RPICommunicator::ok2StopAdvancing(){
  return  (advancing && (delMgr->delayExpired()));
}
void RPICommunicator::prependSpaces(){
  for (int i = 0; i< nbMesg ; i++){
    int len = i == 0 ? 0 : mVec[i-1].length();
    for (int j=0;j<len;j++){
      mVec[i] = ' ' + mVec[i];
    }
  }
}
*/


/*
void RPICommunicator::mainLoop(){
  if (ok2StartAdvancing()){
    sayIt(rpiReadyI);
    startAdvancing();
  }
  if (ok2StopAdvancing()){
    stopAdvancing();
  }
}
*/

/*
void RPICommunicator::sayIt(int msgID){
  Serial.println(mVec[msgID]);   
  delay(WAIT_AFTER_WRITE);
}


void RPICommunicator::startAdvancing(){
  advancing = true;
  writePinAndWait(ok2PersonalizPin,advancing);
  sayIt(advancingI);
  delMgr->start();
}

void RPICommunicator::stopAdvancing(){
  writePinAndWait(ok2PersonalizPin, endOfAdvance);
  sayIt(advanceEndI);
  advancing = false;
  sayIt(waitingI);
}

boolean RPICommunicator::ok2StartAdvancing(){
  return ((digitalRead(ok2AdvancePin) == ready2Advance) && !advancing);
}
*/
