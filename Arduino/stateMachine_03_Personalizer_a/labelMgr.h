#ifndef LABEL_MGR_H
#define LABEL_MGR_H

#include <Arduino.h>
#include "config.h"
#include "tools.h"

class LabelMgr{
  protected:              
    unsigned int stepCount     = 0,
                 lastStepCount = 0,
                 labelCount    = 0;

   
    
    const String labelCountName =  String("Label Count   : ");

    String failedLabelStringList = String("");   // comma separated list of label indices of labels that faile to personalize
 
  public:
    LabelMgr();
    void    init();
    void    incLabelCount();
    void    incStepCount();
    void    resetStepCount();
    float   getSpeedMultiplier() const;
    void    showLabelCount() const;
    void    markCurrentAsFailed();
    void    showFailed() const;

    
};

    /*
    // these are where the step count is maintained
    static const unsigned int labelIndex,
                              gapIndex;
    
    int          labelCountTarget = LABEL_COUNT_TARGET;
    // these are for display
    String countNameVec[2],
           stepNameVec[2];
    void _initNameVecs();
    */
         
    //boolean labelCountAttained() const;
    //void    setLabelLimit(int lim);
    //int     getLabelLimit() const;
    //void    showLabelLimit() const;
    //void    updateCounts(int newIndex);
    //void    finishCountUpdate(int newIndex);
    //float   getSpeedMultiplier(int newIndex) const;
    //void    incIndex(unsigned int index);
    //void    showCounts() const;


#endif
