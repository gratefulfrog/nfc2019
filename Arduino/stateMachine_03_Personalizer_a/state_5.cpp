#include "state.h"

// "State 5: USER ABORT"

const int State::state5nextState = 0;

void State::e5(){
  showName();
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("entry      action  : 5 USER ABORT");
    delay(OBS_DELAY);
  #endif
  Serial.println("\nAborting!\n\n");
  #ifdef DEBUG_STATE_MACHINE
    delay(OBS_DELAY);
  #endif
}

void State::d5(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("during     action  : 5 USER ABORT");
    delay(OBS_DELAY);
  #endif
}

boolean State::t5(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("transition event   : 5 USER ABORT return false");
    delay(OBS_DELAY);
  #endif
  return true;
}

void State::x5(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("exit       action  : 5 USER ABORT");
    delay(OBS_DELAY);
  #endif
}
