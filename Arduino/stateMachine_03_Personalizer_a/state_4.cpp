#include "state.h"

//"State 4: Advance to next label",


const int State::state4nextState = 3;

void State::e4(){
  showName();
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("entry      action  : 4  Advance to next label set nok to pers, inc label count");
    //delay(OBS_DELAY);
  #endif
  dMgr->setNotOK2Personalize();
  /*
  dMgr->incLabelCountAndUpdateMotorSpeedResetStepCount();
  dMgr->showLabelCount();
  dMgr->showBadLabelString();
  delay(OBS_DELAY*3);
  */
}

void State::d4(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("during     action  : 4  Advance to next label  stepOnce(true);");
    delay(OBS_DELAY);
  #endif
  dMgr-> stepOnce(true);
}

boolean State::t4(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("transition event   : 4  Advance to next label dMgr-> gapThenLabelDetected()");
    delay(OBS_DELAY);
  #endif
  return dMgr-> gapThenLabelDetected();
}

void State::x4(){
  #ifdef DEBUG_STATE_MACHINE
    Serial.println("exit       action  : 4 Advance to next label");
    delay(OBS_DELAY);
  #endif
  dMgr->setupForStopStepping();
}
