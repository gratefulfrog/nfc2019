#include "config.h"

void setup(){
  Serial.begin(SERIAL_BAUD_RATE);
  while(!Serial);
  delay(100);
  pinMode(RELAY_PIN, OUTPUT);
}

void loop(){
  digitalWrite(RELAY_PIN, !digitalRead(RELAY_PIN));
  Serial.println(String("Relay pin : ") + String(digitalRead(RELAY_PIN) ? "HIGH" : "LOW"));
  delay(1000);
}
