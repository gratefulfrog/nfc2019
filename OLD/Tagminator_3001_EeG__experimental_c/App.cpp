#include "App.h"

void    App::_showLabelLimit(){
  Serial.println(String("Label Limit : ") + String(labelCountTarget));
}
void    App::_setLabelLimit(int limit){
  labelCountTarget = limit;
}

void App::_showDirection(){
  Serial.println(String("Direction   : ") + String(motor->getDirection() == FORWARD_DIRECTION ? "FORWARD" :  "REVERSE"));  
}

void App::_showStatus(){
  _showLabelLimit();
  _showDirection();
  delayMS(100);
}

void    App::_initNameVecs(){
  countNameVec[labelIndex]   = "Label Count : ";
  countNameVec[gapIndex]     = "Gap   Count : ";
  stepNameVec[labelIndex]    = "Label Steps : ";
  stepNameVec[gapIndex]      = "Gap   Steps : ";
}
void    App::_initCounts(){
  for (int i=0;i<2;i++){
    stepVec[i] = lastStepVec[i] = countVec[i] = 0;   
  }  
}

void    App::_initPins(){
  pinMode(labelSensorPin, INPUT);
}

void    App::_startupMessage(){
  _showLabelLimit();
  Serial.print(String("Press :\n* ") + 
               String(forwardChar) +
               String(" : to wind forward\n* ") +
               String(backChar) +
               String(" : to rewind\n* ") +
               String(limitChar) +
               String(" to set label count limit\nEnter a value  : "));
}

boolean App::_okToStart(){
  static boolean firstTime=true;
  boolean res = false;
  if (firstTime){
    _startupMessage();
    firstTime=false;
  }
  if((Serial.available() > 0)){
    char input= char(Serial.read());
    Serial.println(input);
    firstTime=true;
    switch(input){
      case forwardChar:
        motor->setForward(true);
        res = true;
        break;
      case backChar:
        motor->setForward(false);
        res = true;
        break;
       case limitChar:
         Serial.print("Enter limit : ");
         while(!Serial.available());
         int lim = Serial.parseInt();
         Serial.println(lim);
         _setLabelLimit(lim);
         break;
    }
  }
  if (res){
    _showStatus();
    delayMS(750);
  }
  return res;
}
void    App::_stepOnce(int index){
 motor->step();
  stepVec[index]++;
  /*
  #ifdef DEBUG
    Serial.println(String("Index: ") + String(index) + String(" : steps : ") + String(stepVec[index]));
  #endif
  */
}

void    App::_incShowReset(int newIndex){
  int vecIndex = !newIndex;
  countVec[vecIndex]++;
  Serial.print(countNameVec[vecIndex]);
  Serial.println (countVec[vecIndex]);
  Serial.print(stepNameVec[vecIndex]);
  Serial.println (stepVec[vecIndex]);
  if (lastStepVec[newIndex] == 0){
    lastStepVec[newIndex] = stepVec[newIndex];
  }
  
  float multiplier = 1;
  if (stepVec[newIndex] != 0){
    multiplier = float(lastStepVec[newIndex])/float(stepVec[newIndex]);
  }
  #ifdef MOTOR_SPEED_DEBUG
    Serial.println(String("Multiplier : ") + String(multiplier));
  #endif
  
  motor->updateSpeed(multiplier);
  lastStepVec[newIndex] = stepVec[newIndex];

  stepVec[newIndex] = 0;
  /*
  #ifdef DEBUG
    Serial.println(String("Index : ") + String(newIndex));
  #endif
  */
}

void    App::_setupForStepping(){
  motor->enable(true);
}

void    App::_setupForStopStepping(){
  motor->enable(false);
  Serial.println("\n\n**** Done! ****\n\n");
}
void    App::_reelRewind(){
  _setupForStepping();
  boolean firstLoop = true;
  int index     = -2,
      lastIndex = -1;
   
  #ifdef DEBUG    
    int count = 0;
    state = 0;
  #endif
  
  while(countVec[labelIndex] < labelCountTarget){
    
    #ifdef DEBUG
      count++;
      if (count==5){
        state = !state;
        count=0;
      }
      //Serial.print("State: ");
      //Serial.println(state);
    #endif
    
    index = state; // low = label; high = gap
    boolean newIndex = (index != lastIndex);
    lastIndex = index;
    if (!firstLoop && newIndex){ //we are at the end of either a label or a gap
      _incShowReset(index);
    }
    firstLoop = false;
    _stepOnce(index);
  }  
  _setupForStopStepping();
}



App::App(int _labelSensorPin, 
         unsigned int _labelCountTarget): 
           labelSensorPin(_labelSensorPin),
           labelCountTarget(_labelCountTarget){
  motor = new Stepper(MOTOR_PIN,
                      ENABLE_PIN,
                      DIR_PIN,
                      RELAY_PIN,
                      FORWARD_DIRECTION,
                      SPEED_DELAY,
                      MOTOR_DELAY);
  _initNameVecs();
  _initPins();
}

void App::mainLoop(){
   if(_okToStart()){
    _initCounts();
    _reelRewind();
  }
}

void App::isr(){
  state = digitalRead(labelSensorPin); // low means label, high means gap!
}
