void setup() {
  // put your setup code here, to run once:

}

void delayMS(unsigned long ms, volatile boolean &Flag){
  unsigned long now=millis();
  while(!Flag && (millis()-now<ms));
}

void loop() {
  // put your main code here, to run repeatedly:

}
