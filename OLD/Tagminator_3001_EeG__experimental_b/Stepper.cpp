#include "Stepper.h"

Stepper::Stepper( int    _motorPin, 
                  int    _enablePin,
                  int    _dirPin,
                  int    _relayPin,
                  int    _forwardDirection,
                  int    _speedDelay,
                  int    _motorDelay): motorPin(_motorPin),
                                       enablePin(_enablePin),
                                       dirPin(_dirPin),
                                       relayPin(_relayPin),
                                       forwardDirection(_forwardDirection),
                                       speedDelay(_speedDelay),
                                       motorDelay(_motorDelay){
  pinMode(motorPin, OUTPUT); 
  pinMode(enablePin, OUTPUT);
  pinMode(dirPin, OUTPUT);   
  pinMode(relayPin, OUTPUT);

  enable(false);
  setForward(true);
  
} 
void Stepper::enable(boolean yes){
  digitalWrite(enablePin, !yes);
  enabled = yes;
  /*
  #ifdef DEBUG
    Serial.println(String("stepping enabled: ") + (yes ? "TRUE" : "FALSE"));
    delay(500);
  #endif
  */
}
 
void Stepper::_setDirection(int dir){
      digitalWrite(dirPin,dir);
      digitalWrite(relayPin,dir);
      currentDirection = dir;
}

void Stepper::setForward(boolean yes){
    _setDirection(yes ? forwardDirection : !forwardDirection);
    /*
    #ifdef DEBUG
      Serial.println(String("Direction set : ") + (yes ? "FORWARD" : "REVERSE"));
      delay(500);
    #endif
    */
}

int Stepper::getDirection(){
  return currentDirection;
}
void Stepper::step(){
  if (!enabled){
    enable(true);
  }
  digitalWrite(motorPin, HIGH);
  delayMicroseconds(speedDelay);                  
  digitalWrite(motorPin, LOW);
  delayMicroseconds(motorDelay);
}

void Stepper::updateSpeed(float delayFactor){
  motorDelay =  round(motorDelay  * delayFactor);
  speedDelay =  round(speedDelay  * delayFactor);
  #ifdef MOTOR_SPEED_DEBUG
    Serial.print("(motorDelay,speedDelay) update : (");
    Serial.print(motorDelay);
    Serial.print(", ");
    Serial.print(speedDelay);
    Serial.println(")");
  #endif
}
