#include "config.h"
#include "App.h"

//Constants
const int labelSensorPin   = LABEL_SENSOR_PIN,
          labelCountTarget = LABEL_COUNT_TARGET;

// Pointer to the App instance
App *app;

// ISR
void labelSensorState() {
  app->isr();
}

void initInterrupts(){
  attachInterrupt(digitalPinToInterrupt(labelSensorPin), labelSensorState, CHANGE);  
}

void setup(){
  Serial.begin(SERIAL_BAUD_RATE);
  while(!Serial);
  delayMS(100);
  app = new App(labelSensorPin,labelCountTarget);
  initInterrupts();
}

void loop(){
  app->mainLoop();
}
