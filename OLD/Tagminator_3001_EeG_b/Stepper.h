#ifndef STEPPER_H
#define STEPPER_H

#include <Arduino.h>
#include "config.h"
#include "Tools.h"

class Stepper{
  protected:
    const int    motorPin, 
                 enablePin,
                 dirPin,
                 relayPin,
                 forwardDirection;

    const double speedDelay,
                 motorDelay;

    int currentDirection = forwardDirection;
    boolean enabled = false;
    
    void _setDirection(int dir);    

  public:
    Stepper(int    _motorPin, 
            int    _enablePin,
            int    _dirPin,
            int    _repalyPin,
            int    _forwardDirection,
            double _speedDelay,
            double _motorDelay);

    void enable(boolean yes);   
    void setForward(boolean yes);
    void step();
    int getDirection();
};
#endif
