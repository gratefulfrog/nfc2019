void setup() {
  Serial.begin(9600);
  while(!Serial);
  delay(500);
  Serial.println("Enter any character to begin print test...\n");
}

const String s ="abcdefghijklmnopqrstuvwxyz 0123456789  ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789";

void loop() {
  while (!Serial.available());
  Serial.println(s);
  delay(1000);
}
