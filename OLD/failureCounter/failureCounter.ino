

String failedIndices = "";

void addFailure(unsigned index){
  failedIndices = failedIndices + String(index) + ",";
}

void showFailed(){
  // just ot be sure we got what we expected
  Serial.println(failedIndices);

  // now print 'em out!
  int nextComma =  failedIndices.indexOf(',');
  int nextStart = 0;
  while (nextComma != -1){
    int failedIndex = failedIndices.substring(nextStart,nextComma).toInt();
    processFailedIndex(failedIndex);
    nextStart = nextComma+1;
    nextComma =  failedIndices.indexOf(',',nextStart);
  }
}

void processFailedIndex(int index){
  // pretend processing, here you could send a goto index message to the motor arduino
  Serial.println(index);
}

void addFailures(int nb){
  for (int i = 0;i<nb; i+=3){
    addFailure(i);
  }
}

void setup() {
  Serial.begin(115200);
  while(!Serial);
  delay(50);
  addFailures(200);
  showFailed();
}

void loop() {
}
