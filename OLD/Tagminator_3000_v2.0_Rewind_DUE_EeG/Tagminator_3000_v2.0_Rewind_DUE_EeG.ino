#include <stdio.h>
#include "config.h"

const unsigned int labelCountTarget = LABEL_COUNT_TARGET;

//Constants
const int motorPin       = MOTOR_PIN, 
          enablePin      = ENABLE_PIN,
          pausePin       = PAUSE_PIN,
          labelSensorPin = LABEL_SENSOR_PIN,
          dirPin         = DIR_PIN,
          relayPin       = RELAY_PIN;
          //holdAfterStop  = HOLD_AFTER_STOP;          

double speedDelay = SPEED_DELAY,
       motorDelay = MOTOR_DELAY;

// this is used in the ISR
volatile int state = 0;

const char forwardChar = '1',
           backChar    = '0';
           
const int labelIndex         = LABEL_DETECTED,
          gapIndex           = !labelIndex,
          forwardPinSetting  = FORWARD_DIRECTION;

unsigned int stepVec[]  = {0,0},
             countVec[] = {0,0};

String countNameVec[2],
       stepNameVec[2];
       
// ISR
void labelSensorState() {
  state = digitalRead(labelSensorPin); // low means label, high means gap!
}

void initNameVecs(){
  countNameVec[labelIndex]   = "Label Count : ";
  countNameVec[gapIndex]     = "Gap   Count : ";
  stepNameVec[labelIndex]    = "Label Steps : ";
  stepNameVec[gapIndex]      = "Gap   Steps : ";
}

void initPins(){
  pinMode(motorPin, OUTPUT); 
  pinMode(enablePin, OUTPUT);
  pinMode(dirPin, OUTPUT);   
  pinMode(relayPin, OUTPUT);   
  
  pinMode(labelSensorPin, INPUT);
  digitalWrite(enablePin, HIGH);
  digitalWrite(dirPin, forwardPinSetting); 
  digitalWrite(relayPin,forwardPinSetting);
  
  attachInterrupt(digitalPinToInterrupt(labelSensorPin), labelSensorState, CHANGE);  
}

void startupMessage(){
  char buff[60];
  sprintf(buff,"Press %c to wind forward, and %c to rewind : ",forwardChar,backChar); 
  Serial.print(buff); 
}

void setup(){
  Serial.begin(SERIAL_BAUD_RATE);
  while(!Serial);
  initNameVecs();
  initPins();
}

boolean okToStart(){
  static boolean firstTime=true;
  boolean res = false;
  if (firstTime){
    startupMessage();
    firstTime=false;
  }
  if((Serial.available() > 0)){
    char input= char(Serial.read());
    Serial.println(input);
    firstTime=true;
    switch(input){
      case forwardChar:
        digitalWrite(relayPin,forwardPinSetting);
        digitalWrite(dirPin,  forwardPinSetting);
        res = true;
        break;
      case backChar:
        digitalWrite(relayPin,!forwardPinSetting);
        digitalWrite(dirPin,  !forwardPinSetting);
        res = true;
        break;
       default:
        res = false; 
        break;
    }
  }
  countVec[labelIndex] = countVec[gapIndex] = 0;
  return res;
}

void stepOnce(int index){
  digitalWrite(motorPin, HIGH);
  delayMicroseconds(speedDelay);                  
  digitalWrite(motorPin, LOW);
  delayMicroseconds(motorDelay);
  stepVec[index]++;
  #ifdef DEBUG
    Serial.println(String("Index: ") + String(index) + String(" : steps : ") + String(stepVec[index]));
  #endif
}

void incShowReset(int index){
  countVec[!index]++;
  Serial.print(countNameVec[!index]);
  Serial.println (countVec[!index]);
  Serial.print(stepNameVec[!index]);
  Serial.println (stepVec[!index]);
  stepVec[index] = 0;
  #ifdef DEBUG
    Serial.println(String("Index : ") + String(index));
  #endif
}

void setupForStepping(){
  digitalWrite(enablePin, LOW);  
}

/*
void delayMS(unsigned long del){
  unsigned long now = millis();
  while (millis()-now < del);
}
*/
void setupForStopStepping(){
  //delayMS(holdAfterStop);
  digitalWrite(enablePin, HIGH);  
  Serial.println("\n\n**** Done! ****\n\n");
}

void reelRewind() {
  setupForStepping();
  boolean firstLoop = true;
  int index     = -2,
      lastIndex = -1;
  
  #ifdef DEBUG    
    int count = 0;
    state = 0;
  #endif

  while(countVec[labelIndex] < labelCountTarget){
  
    #ifdef DEBUG
    count++;
    if (count==5){
      state = !state;
      count=0;
    }
    Serial.print("State: ");
    Serial.println(state);
    #endif

    index = state; // low = label; high = gap
    boolean newIndex = (index != lastIndex);
    lastIndex = index;
    if (!firstLoop && newIndex){ //we are at the end of either a label or a gap
      incShowReset(index);
    }
    firstLoop = false;
    stepOnce(index);
  }  
  setupForStopStepping();
}

void loop(){
  if(okToStart()){
    reelRewind();
  }
}
