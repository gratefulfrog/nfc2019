#!/usr/bin/python3

serverIP   = '192.168.0.23'
serverPort = 50007

import socket
import struct
import time
import sys

structFmt = '>bbb'
outgoing = [ord(x) for x in ['a','b','c']]

index= 0

def getMsg():
    global index
    res = bytes(str(index),'utf8')
    index+=1
    return res

def betterClient(killer):
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((serverIP, serverPort))
            if killer:
                s.sendall(bytes('k','utf8'))
            else:
                while True:
                    s.sendall(getMsg())
                    time.sleep(3)
    except:
        pass
    finally:
        print('Bye!')
        
if __name__ == '__main__':
    kill = False
    if len(sys.argv)>1:
        kill = True
    betterClient(kill)
    
