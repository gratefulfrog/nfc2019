#!/usr/bin/python3

serverIP   = ''
serverPort = 50007
socketTimeout = 1

import socket
import struct
import threading
import time

class WorkerThread(threading.Thread):
    def __init__(self,conn,index,abortEvent,isKiller=False):
        threading.Thread.__init__(self)
        self.daemon     = True
        self.conn      = conn
        self.index      = index
        self.abortEvent = abortEvent

    def run(self):
        count = 0
        with self.conn:
            while not self.abortEvent.is_set():
                data = self.conn.recv(1024)
                if not data:
                    continue
                elif data == bytes('k','utf8'):
                    self.abortEvent.set()
                print('Thread:',self.index,'step:',count,'received: ', data)
                count+=1
        print('Thread:',self.index,'exiting...')
        
        
structFmt = '>bbb'

def decodeMsg(msg):
    return [chr(x) for x in struct.unpack(structFmt,msg)]

def get_ip_address():
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        s.connect(("8.8.8.8", 80))
        return s.getsockname()[0]

def betterServer():
    abort =  threading.Event()
    threadIndex = 0
    threadVec = []
    print('Server IP Address:',get_ip_address())
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        #s.settimeout(socketTimeout)
        s.bind((serverIP, serverPort))
        s.listen(1)
        
        while True:
            print('waiting for a connection')
            try:
                conn, addr = s.accept()
            except socket.timeout:
                #print('socket timed out')
                continue
                
            print('Connected by', addr)
            thr = WorkerThread(conn,threadIndex,abort)
            threadIndex += 1
            threadVec.append(thr)
            thr.start()
            time.sleep(3)
            if abort.is_set():
                for th in threadVec:
                    th.join()
                return
            
if __name__ == '__main__':
    betterServer()
            
