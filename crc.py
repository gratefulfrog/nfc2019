#!/usr/bin/python3

import sys

def crcIt(argVec):
    crc = 0xFFFF

    for j in range(len(argVec)):
        crc = ((crc >> 8) | (crc << 8)) & 0xffff
        crc ^= (argVec[j] & 0xff) 
        crc ^= ((crc & 0xFF) >> 4)
        crc ^= (crc << 12) & 0xFFFF
        crc ^= ((crc & 0xFF) << 5) & 0xFFFF
  
    crc &= 0xFFFF;
    #print(hex(crc>>8 & 0xFF))
    #print (hex(crc & 0XFF))
    return hex(crc)

if __name__ == '__main__':
    if (len(sys.argv)) < 2:
        print('Usage:')
        print('./crc.py 0xAF 0x00')
        print('any number of arguments is ok')
    else:
        print(crcIt([int(s,16) for s in sys.argv[1:]]))
    
