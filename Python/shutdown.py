#!/usr/bin/python3

"""
To shutdown the headless RPi, just ground GPIO PIN 26, RPi pin 37, 
In other words touch the last 2 pins on the inside row together, the pins
at the end nearest the USB ports

But be sure to edit the file path and copy 'shutdown.desktop' to
/home/pi/.config/autostart
and configure auto-login (that is the default)

""" 

import time
import subprocess
import RPi.GPIO as GPIO


pin = 26

def onFall(unusedPin):
    subprocess.run(['sudo','halt'])
    
def waitForGnd():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin, GPIO.IN, pull_up_down = GPIO.PUD_UP)
    GPIO.add_event_detect(pin,GPIO.FALLING,onFall)
    while True:
        time.sleep(1e6)
        
if __name__ == '__main__':
    waitForGnd()
