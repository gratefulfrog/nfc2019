#!/usr/bin/python3

import config
from delayMgr import DelayMgr

if config.DEBUG:
    import gpio as GPIO
else:
    import RPi.GPIO as GPIO

from time import sleep


class Personalizer(object):
    waitingForTagy       = 0
    tagyIsReady          = 1
    doingPersonalization = 2
    personalizationEnded = 3
    
    mVec = ['Waiting for Tagy to finish advancing...',
            'Tagy is Ready for Personalization',
            'Personalizing...',
            'Personalization ended!']
    
    def __init__(self,
                 inPin = config.inPin,
                 outPin = config.outPin,
                 badLabelPin = config.badLabelPin):
        print('Started!')
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        self.addSpaces()
        self.inPin             = inPin
        GPIO.setup(self.inPin, GPIO.IN)
        self.outPin            = outPin
        self.badLabelPin       = badLabelPin
        self.pTime             = config.persTime
        self.saidIt            = False
        self.personalizing     = False
        self.endOfPersSignal   = config.readySignal
        self.ready2PersSignal  = config.readySignal
        self.badLabelSignal    = config.readySignal
        self.delayMgr          = DelayMgr(self.pTime)
        self.spaces            = 0
        GPIO.setup(self.outPin, GPIO.OUT, initial= config.readySignal)
        GPIO.setup(self.badLabelPin, GPIO.OUT, initial= not self.badLabelSignal)
        self.sayIt(Personalizer.waitingForTagy)
        self.count = 0

    def addSpaces(self):
        for i in range(len(self.mVec)):
            self.mVec[i] = ' '* (0 if i == 0 else len(self.mVec[i-1])) + self.mVec[i]

    def mainLoop(self):
        if self.ok2StartPersonalizing():
            self.saidIt = False
            self.sayIt(Personalizer.tagyIsReady)
            self.startPersonalizing()
        if self.ok2StopPersonalizing():
            self.stopPersonalizing()

    def sayIt(self, index):
        sleep(0.5)
        print(self.mVec[index])
        
    def startPersonalizing(self):
        self.personalizing = True
        GPIO.output(self.outPin, self.personalizing)
        self.sayIt(Personalizer.doingPersonalization)
        if self.count %2:
            print('Label : ' + str(self.count) + ' failed!')
            GPIO.output(self.badLabelPin, self.badLabelSignal)
        self.count += 1
        self.delayMgr.start()

    def stopPersonalizing(self):
        self.personalizing = False
        self.sayIt(Personalizer.personalizationEnded)
        GPIO.output(self.outPin, self.endOfPersSignal)
        sleep(0.5)
        GPIO.output(self.badLabelPin, not self.endOfPersSignal)
        self.sayIt(Personalizer.waitingForTagy)

    def ok2StartPersonalizing(self):
        return ((GPIO.input(self.inPin) == self.ready2PersSignal) \
                and (not self.personalizing))

    def ok2StopPersonalizing(self):
        return  (self.personalizing and self.delayMgr.delayExpired())

