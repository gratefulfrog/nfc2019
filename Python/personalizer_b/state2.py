import stateMachine 
import time
import config

nextStateIndex  = 1

name  = 'Personalizing'
badFound = False

def entryAction():
    #print(stateMachine.State.stateVec[2].name)
    if config.DEBUG_STATE_MACHINE:
        print(stateMachine.State.stateVec[2].name + ': Entry Action')
        time.sleep(0.5)
    stateMachine.State.dMgr.setBadLabelFlag(False)
    stateMachine.State.dMgr.setOk2AdvanceFlag(False)

def duringAction():
    global badFound
    stateMachine.State.dMgr.personalize()
    if ((not badFound) and stateMachine.State.dMgr.badLabelDetected()):
        stateMachine.State.dMgr.setBadLabelFlag(True)
        badFound = True
    
def transitionTest():
    return stateMachine.State.dMgr.personalizationDone()

def exitAction():
    global badFound
    badFound = False
    stateMachine.State.dMgr.stopPersonalization()
    stateMachine.State.dMgr.showCounts()

    
