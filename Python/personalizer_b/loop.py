#!/usr/bin/python3

import config
if config.DEBUG:
    import gpio as GPIO
else:
    import RPi.GPIO as GPIO

from time import sleep

#pins = [17,22,23,27]

def test():
    GPIO.setmode(GPIO.BCM)
    #GPIO.setwarnings(False)
    for i in config.allPins:
        print('Seting up pin: ' + str(i))
        GPIO.setup(i, GPIO.OUT, initial= 0)
        print('Pin: ' + str(i) + ' set up ok')
    try:
        while True:
            for i in config.allPins:
                GPIO.output(i, not GPIO.input(i))
            sleep(1)
    except KeyboardInterrupt:
        print('\nEnded!')
    finally:
        GPIO.cleanup()

if __name__ == '__main__':
    test()
