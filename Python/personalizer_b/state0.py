import stateMachine 
import time
import config

nextStateIndex  = 1

name  = 'Initialization'

def entryAction():
    print(stateMachine.State.stateVec[0].name)
    if config.DEBUG_STATE_MACHINE:
        print(stateMachine.State.stateVec[0].name + ': Entry Action')
        time.sleep(0.5)
    stateMachine.State.dMgr.reset()
    stateMachine.State.dMgr.setBadLabelFlag(False)
    stateMachine.State.dMgr.setOk2AdvanceFlag(False)

def duringAction():
    stateMachine.State.dMgr.acquireNbLabels2Personalize()
    
def transitionTest():
    res = stateMachine.State.dMgr.nbLabelsAcquired()
    #print('transition test : ' + str(res))
    time.sleep(1)
    return res

exitAction = None
