#!/usr/bin/python3

DEBUG               = False #True
DEBUG_STATE_MACHINE = False # True
DEBUG_PINS          = False #True

"""
To run this on an RPI:
* connect a pushbutton with
1 contact to GND,
1 contact to RPi BCM pin 2

A LED 
+pin to RPi BCM pin, 2, 
-pin to 1K R connected to  GND

Another LED:
+pin to RPi BCM pin 3
-pin to 1K R connected to GND

If other pins are used, they must be configured below
"""

# this pin will be pulled LOW by the Arduino when
# it has moved a label into position for personalizing
ok2PersonalizePin = 23

# this pin will be pulled LOW by the RPi when
# it has finished personalizing the current label
ok2AdvancePin     = 17

# this pin will be pulled LOW by the RPi when
# Rpi detects a bad label
badLabelPin       = 27

# this pin will be pulled LOW by the USER to Abort processing
abortPin          = 22

# This indicates what the ready signal shall be, i.e 0 if LOW, 1 if HIGH
readySignal = 0

# this is just a simulation parameter representing the time
# the real pesonalization would take, units are seconds, decimals are ok 
persTime    = 5

# this is the time we wait after setting a pin with wait==True
pinWaitDelay = 1

# do not touch this line
inPins =  [ok2PersonalizePin, abortPin]
outPins = [ok2AdvancePin, badLabelPin]
