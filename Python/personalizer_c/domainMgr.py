
from time import time, sleep

import config
if config.DEBUG:
    import gpio as GPIO
else:
    import RPi.GPIO as GPIO


class DomainMgr(object):

    def __init__(self):
        GPIO.setmode(GPIO.BCM)
        self.signal            = config.readySignal

        # output pins
        self.ok2PersonalizePin = config.ok2PersonalizePin
        self.badLabelPin       = config.badLabelPin
        for p in config.outPins:
            GPIO.setup(p, GPIO.OUT, initial = not self.signal)

        #input pins
        self.ok2AdvancePin     = config.ok2AdvancePin
        self.resetPin          = config.resetPin
        for p in config.inPins:
            if config.DEBUG_PINS:
                GPIO.setup(p, GPIO.IN, pull_up_down = GPIO.PUD_UP)
            else:
                GPIO.setup(p, GPIO.IN)

        self.personalizationWait    = config.persTime
        self.reset()

    def reset(self):
        self.setBadLabelFlag(False)
        self.setOk2AdvanceFlag(False)
        self.labelCount             = 0
        self.badLabelList           = []
        self.nbLabels2Personalize   = 0
        self.personalizing          = False
        self.startPersonalizingTime = None
        self.callForReset(True)
        
    def _setFlag(self, pin, value, wait):
        GPIO.output(pin, value)
        if wait:
            sleep(config.pinWaitDelay)

    def setBadLabelFlag(self, value):
        if value:
            print('Bad Label Detected!')
            self.badLabelList.append(self.labelCount)
        self._setFlag(self.badLabelPin,
                      (self.signal if value else not self.signal),
                      False)
        
    def setOk2AdvanceFlag(self, value, wait = True):
        self._setFlag(self.ok2AdvancePin,
                      (self.signal if value else not self.signal),
                      wait)

    def acquireNbLabels2Personalize(self):
        sRead = input('Input number of labels to personalize : ')
        try:
            self.nbLabels2Personalize = int(sRead)
        except:
            pass

    def nbLabelsAcquired(self):
        return (self.nbLabels2Personalize > 0)

    def nbLabelsAttained(self):
        return (self.labelCount == self.nbLabels2Personalize)
    
    def ok2Personalize(self):
        return (GPIO.input(self.ok2PersonalizePin) == self.signal)

    def personalize(self):
        if not self.personalizing:
            self.personalizing = True
            self.startPersonalizingTime = time()
            print('Personalizing label : ' + str(self.labelCount))

    def badLabelDetected(self):
        # let's make the odd not just 'odd' and bad as well
        return (self.labelCount%2)
    
    def personalizationDone(self):
        return ((time() - self.startPersonalizingTime) > self.personalizationWait) 
        
    def showCounts(self):
        self.showLabelCount()
        self.showBadLabelList()
        
    def showLabelCount(self):
        print('Number of labels    : ' + str(self.labelCount))
        
    def showBadLabelList(self):
        print('List of BAD labels  : ' + str(self.badLabelList))

    def stopPersonalization(self):
        self.labelCount +=1
        self.personalizing = False

    def callForReset(self,value):
        self._setFlag(self.resetPin, not value, True)
