#!/usr/bin/python3
import time

import config

import state0
import state1
import state2
#import state3
import state4

from domainMgr import DomainMgr

if config.DEBUG:
    import gpio as GPIO
else:
    import RPi.GPIO as GPIO


class State(object):
    startStateIndex  = 0
    #abortStateIndex  = 3
    #abortState       = None
    startState       = None
    dMgr             = None
    
    stateVec = []
    def init():
        State.dMgr = DomainMgr()
        sourceStateData = [state0,state1,state2,state4] #,state3,state4]
        for s in sourceStateData:
            State.stateVec.append (State(s.name,
                                         None,
                                         s.entryAction,
                                         s.duringAction,
                                         s.transitionTest,
                                         s.exitAction))
        for i in range(len(State.stateVec)):
            State.stateVec[i].nextState = State.stateVec[sourceStateData[i].nextStateIndex]
            
        State.startState = State.stateVec[State.startStateIndex]
        if config.DEBUG_STATE_MACHINE:
            print(State.startState.name)
        """
        State.abortState = State.stateVec[State.abortStateIndex]
        if config.DEBUG_STATE_MACHINE:
            print(State.abortState.name)
        """
        
    def __init__(self, name, nextState, entryAction, duringAction,transitionTest,exitAction):
        self.name           = name
        self.nextState      = nextState
        self.entryAction    = entryAction
        self.duringAction   = duringAction
        self.transitionTest = transitionTest
        self.exitAction     = exitAction

    def showName(self):
        print(self.name)


class StateMachine(object):

    def __init__(self):
        #self.abortPin = config.abortPin
        State.init()
        self.currentState = State.startState
        self.lastState    = None
        if config.DEBUG_STATE_MACHINE:
            print('New State Machine!')
            print(self.currentState.name)
            time.sleep(1)

    def mainLoop(self):
        #if self._abortRequested():
        #    self._doAbort()
        if self._stateChanged():
            self._updateLastState()
            self._execAction(self.currentState.entryAction)
        self._execAction(self.currentState.duringAction)
        if self.currentState.transitionTest():
            if config.DEBUG_STATE_MACHINE:
                print('Exiting state: ' + self.currentState.name)
            self._execAction(self.currentState.exitAction)
            self._updateCurrentState()

    def _execAction(self, action):
        if action:
            action()
            
    def _stateChanged(self):
        return self.currentState != self.lastState
    
    def _updateLastState(self):
        if config.DEBUG_STATE_MACHINE:
            print('\n State changed!')
            print(self.currentState.name)
            time.sleep(1)
        self.lastState =  self.currentState

    def _updateCurrentState(self):
        self.currentState = self.currentState.nextState
