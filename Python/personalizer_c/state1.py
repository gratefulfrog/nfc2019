import stateMachine 
import time
import config

nextStateIndex  = 2

name  = 'Waiting for Tagy to position label'

def entryAction():
    if config.DEBUG_STATE_MACHINE:
        print(stateMachine.State.stateVec[1].name + ': Entry Action')
        time.sleep(0.5)
    if stateMachine.State.dMgr.nbLabelsAttained():
        stateMachine.State.stateVec[1].nextState = stateMachine.State.stateVec[3]
    else:
        print(stateMachine.State.stateVec[1].name)
        stateMachine.State.stateVec[1].nextState = stateMachine.State.stateVec[2]
    stateMachine.State.dMgr.setOk2AdvanceFlag(True)

duringAction = None
    
def transitionTest():
    res = (stateMachine.State.dMgr.ok2Personalize() or stateMachine.State.dMgr.nbLabelsAttained())
    if config.DEBUG_STATE_MACHINE:
        print(name + ' ' + str(res))
        time.sleep(0.5)
    return res

exitAction = None
