#!/usr/bin/python3

import config

if config.DEBUG:
    import gpio as GPIO
else:
    import RPi.GPIO as GPIO

from stateMachine import StateMachine

def run():
    sm =  StateMachine()
    while True:
        sm.mainLoop()

if __name__ == '__main__' :
    try:
        run()
    except KeyboardInterrupt:
        print('\nEnded!')
    finally:
        GPIO.cleanup()

