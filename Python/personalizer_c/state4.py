import stateMachine 
import time
import config

nextStateIndex  = 0

name  = 'Clearing last label'

def entryAction():
    print('Waiting for Tagy to clear last label')

duringAction = None
    
def transitionTest():
    return stateMachine.State.dMgr.ok2Personalize()

def exitAction():
    print('\n\nAll labels processed')
    stateMachine.State.dMgr.showCounts()

