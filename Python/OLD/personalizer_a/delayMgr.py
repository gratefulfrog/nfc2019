#!/user/bin/python3

from time import time

class DelayMgr(object):
    def __init__(self, delay):
        self.delay = delay
        self.startTime = time()

    def start(self):
        self.startTime = time()

    def delayExpired(self):
        return (time()-self.startTime >= self.delay)
        
