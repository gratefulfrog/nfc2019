#!/usr/bin/python3

import RPi.GPIO as GPIO

import config

from personalizer import Personalizer 

def run():
    rpi =  Personalizer()
    while True:
        rpi.mainLoop()

if __name__ == '__main__' :
    try:
        run()
    except KeyboardInterrupt:
        print('\nended!')
    finally:
        GPIO.cleanup()

