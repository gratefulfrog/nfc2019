#!/usr/bin/python3

DEBUG = False #True

"""
To run this on an RPI:
* connect a pushbutton with
1 contact to GND,
1 contact to RPi BCM pin 2

A LED 
+pin to RPi BCM pin, 2, 
-pin to 1K R connected to  GND

Another LED:
+pin to RPi BCM pin 3
-pin to 1K R connected to GND

If other pins are used, they must be configured below
"""

# this pin will be pulled LOW by the Arduino when
# it has moved a label into position for personalizing
inPin = 17


# this pin will be pulled LOW by the RPi when
# it has finished personalizing the current label
outPin     = 22

# this pin will be pulled LOW by the RPi when
# Rpi detects a bad label
badLabelPin       = 23

# this pin will be pulled LOW by the USER to Abort processing
abortPin          = 27

# This indicates what the ready signal shall be, i.e 0 if LOW, 1 if HIGH
readySignal = 0

# this is just a simulation parameter representing the time
# the real pesonalization would take, units are seconds, decimals are ok 
persTime    = 10

# do not touch this line
allPins = [inPin, outPin, badLabelPin]
outPins = [outPin, badLabelPin]
