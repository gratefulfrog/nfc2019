#!/usr/bin/python3

import RPi.GPIO as GPIO

import config

from personalizer import Personalizer 

def run():
    rpi =  Personalizer()
    while True:
        rpi.mainLoop()

if __name__ == '__main__' :
    try:
        run()
    except:
        print('\nended!')
    finally:
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(config.outPin, GPIO.OUT, initial= not config.readySignal)
        #GPIO.output(config.outPin, not config.readySignal)
