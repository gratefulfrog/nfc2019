
// this will generate a CRC for any number of bytes
// just set the values below correctly and the CRC will be displayed in the Serial Monitor

uint8_t nbBytes        = 2; //8  ;
uint8_t outgoing[] = {0xAF, 0x00}; //{0x07, 0xCF, 00, 0x09, 0x08, 00, 00, 00}; 

//{0x0D, 0x00,   0x00, 0x07, 0xCF, 0x00, 0x09, 0x08, 0x00, 0x00, 0x00}; 



uint16_t crc16(uint8_t incoming[], uint8_t nbBy){
  uint16_t crc = 0xFFFF;
  for (int j = 0; j < nbBy ; j++)  {
    crc =  (((crc >> 8) | (crc << 8)) & 0xFFFF);
    crc ^= (incoming[j] & 0xFF);//byte to int, trunc sign
    crc ^= (((crc & 0xFF) >> 4));
    crc ^= ((crc << 12) & 0xFFFF);
    crc ^= (((crc & 0xFF) << 5) & 0xFFFF);
  }
  crc &= 0xFFFF;
  return crc;
}


void setup() {
  Serial.begin(115200);
  while(!Serial);
  
  uint16_t crc = crc16(outgoing,nbBytes);
  uint16_t byteL = crc>>8 & 0xFF;
  Serial.print(byteL, HEX);
  Serial.print(" ");
  byteL = crc & 0xFF;
  Serial.println(byteL, HEX);
}


void loop() {
}
