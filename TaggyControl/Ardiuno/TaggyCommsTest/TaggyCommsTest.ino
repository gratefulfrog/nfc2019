// Taggy Comms Test
/* Some experiments in Taggy Comms
 * 
 * This will eventually need an Arduino Mega:DUE etc. since the 
 * Uno does not have multiple serial ports.
 */

#define DEBUG
#define LITTLE_ENDIAN

#ifdef LITTLE_ENDIAN
const boolean rendify = true;
#else
const boolean rendify = false;
#endif

struct __attribute__((packed))  messageStruct{
  char      commandLetter;    // char x (abort), p (personalieze, w, (wind), r (rewind), i (info), r (reset)
  uint32_t  numVal;           // 0 – 0xFFFFFFFF
};

const long baudRate                = 115200;

void littleEndify(uint8_t *src, uint8_t *targ, int len){
  for (int i=0;i<len;i++){
    targ[i] = src[len-i-1]; 
  }
}

uint32_t littleEndifyI(uint32_t val, uint8_t size){
  uint32_t  res =  0; 
  for (uint8_t i =0; i<size; i++){
    res |= ((val>>8*i) & 0xFF)<<(8*(size-i-1));
  }
  return res;
}


uint16_t get16ReEndify(){
  int index = 0;
  boolean started = false;
  uint8_t incoming[2];
  while (true){
    if (Serial.available()>0){
      incoming[index] = Serial.read();
      started = true;
      index ^=1;
  }
    if (started && not index){
      return(((uint16_t)(incoming[rendify ? 1 : 0]<<8)) | incoming[rendify ? 0 : 1]);
    }
  }
}

uint32_t get32ReEndify(String s =  ""){  // string reading is aimed at debugging
  uint8_t incoming[4];
  if (s.equals("")){ 
    int index = 0;
    boolean started = false;
    while (true){
      if (Serial.available()>0){
        incoming[index] = Serial.read();
        started = true;
        index = (index+1)%4;
      }
      if (started && not index){
        break;
      }
    }
  }
  else{  // we have a string
    uint32_t incomingNum = s.toInt();
    uint8_t inVec[4];
    for (int i=0;i<4;i++){
      incoming[i] = (uint8_t) (incomingNum>>(i*8) & 0xFF);
    }
  }
  // here we do the reversal for internal processing
  uint32_t res = 0;
  for (uint8_t i=0;i< 4;i++){
    res |= (uint32_t)(incoming[i])<<((rendify ? 3-i : i)*8);
  }
  return res;
}

uint8_t get8(char c = '\0'){  // no need to Endify only one byte, Duh!
  if (c){
    return c;
  }
  uint8_t incoming;
  while (true){
    if (Serial.available()>0){
      incoming = Serial.read();
      return (incoming);
    }
  }
}

void showInS(const messageStruct &inS){
  Serial.print(inS.commandLetter);
  Serial.println(inS.numVal,HEX);
}

void readQuery(messageStruct &inS, char c =  '\0', String s = ""){
  inS.commandLetter = (char)get8(c);
  inS.numVal = get32ReEndify(s);
}

void replyQuery(const messageStruct &inS){
  switch(inS.commandLetter){
    case 'x':
    case 'p':
    case 'w':
    case 'r':
    case 'i':
    case 'z':
      showInS(inS);
      break;
  }
}

void send32(uint32_t s){
  // Serial.print("Replying: ");
  // Serial.println(s,DEC);
  for (int i = 0;i<4;i++){
    Serial.write((s>>(8*i)) & 0xFF);
  }
}

void send16(uint16_t s){
  // Serial.print("Replying: ");
  // Serial.println(s,DEC);
  Serial.write(s & 0xFF);
  Serial.write((s >>8) & 0xFF);
}

void send8(uint8_t s){
  // Serial.print("Replying: ");
  // Serial.println(s,DEC);
  Serial.write(s);
}

void replyFFFF(){
  static const uint16_t ffff = 0xFFFF;
  send16(ffff);
}

void setup() {
  Serial.begin(baudRate); // this is we read commands and write responses, this is the connection to the computer with the GUI
  while(!Serial);

  uint16_t x =  0xF000;
  Serial.println(x,HEX);
  uint8_t y = (uint8_t)(x>>8);
  Serial.println(y,HEX);

  char      c  = 'x';
  uint32_t  v  = 0xFF;
  
  messageStruct msg;
  msg.commandLetter = c;
  msg.numVal = 0x12AB34CD;
  showInS(msg);
  uint32_t r = get32ReEndify(String(msg.numVal));
  Serial.println(r,HEX);
}

void loop() {
  #ifdef DEBUG
  while (!Serial.available());
  char c = Serial.read();
  String s = Serial.readString();
  Serial.println(c);
  Serial.println(s);
  messageStruct msg;
  msg.commandLetter = c;
  msg.numVal = get32ReEndify(s);
  showInS(msg);
  #else
  if (Serial.available()>0){
    incomingStruct inS; 
    readQuery(inS);
    //Serial.println("Received Query:");
    showInS(inS);
    replyQuery(inS);
  }
  #endif
} 


/*
struct incomingStruct{
  uint16_t length;
  uint8_t  mode;
  uint8_t  content[3];
  uint16_t crc;
};

struct __attribute__((packed)) replyStruct{
  uint8_t   componentId;      // 0x07
  uint8_t   reportId;         // Get: 0xC7
  uint16_t  versionBCD;       // 0xnndd
  uint16_t  modelRevBCD;      // 0xnndd
  char      productName[50];  // "Kromek D3S" (ASCII)
  char      serialNumber[50]; // AAAnnnnnn (ASCII)
} ;

struct __attribute__((packed)) KromekSerialAboutReport{
  uint8_t   componentId;      // 0x07
  uint8_t   reportId;         // Get: 0xC7
  uint16_t  versionBCD;       // 0xnndd
  uint16_t  modelRevBCD;      // 0xnndd
  char      productName[50];  // "Kromek D3S" (ASCII)
  char      serialNumber[50]; // AAAnnnnnn (ASCII)
} ;

struct __attribute__((packed)) KromekSerialAboutReport{
  uint8_t   componentId;      // 0x07
  uint8_t   reportId;         // Get: 0xC7
  uint16_t  versionBCD;       // 0xnndd
  uint16_t  modelRevBCD;      // 0xnndd
  char      productName[50];  // "Kromek D3S" (ASCII)
  char      serialNumber[50]; // AAAnnnnnn (ASCII)
} ;

struct __attribute__((packed)) KromekSerialSpectrumReport{
  uint8_t   componentId;          // 0x07
  uint8_t   reportId;             // Get: 0xC1
  uint32_t  realTime_ms;          // 0 – 0xFFFFFFFF
  uint16_t  neutronCounts;    
  uint16_t  gammaSpectrum[DEBUG_SIZE];  // 0 - 0xFFFF per channel
} ;
  
const KromekSerialAboutReport about = {0x07,      // = 7
                                       0xC7,      // = 199
                                       0x1234,    // = 4660
                                       0x5678,    // = 22136
                                       "Kromek D3S",
                                       "ABC12345"};
KromekSerialSpectrumReport spectrum;        

void updateSpectrumReport (KromekSerialSpectrumReport &s){
  s.componentId   = 0x07;
  s.reportId      = 0xC1;
  s.realTime_ms   =  getDeltaMS(); // (uint32_t)millis();
  s.neutronCounts =  42;
  for (int i =0;i<DEBUG_SIZE;i++){
    s.gammaSpectrum[i] = (uint16_t)i; 
  }
}

void replySpectrum(){
  updateSpectrumReport(spectrum);
  send16(sizeof(spectrum)+5);
  send8(0);
  send8(spectrum.componentId);      // 0x07
  send8(spectrum.reportId);         // Get: 0xC1
  send32(spectrum.realTime_ms);
  send16(spectrum.neutronCounts);
  for (int j =0; j< DEBUG_MULTIPLIER;j++){
    for(int i=0;i<DEBUG_SIZE;i++){
      send16(spectrum.gammaSpectrum[i]);
    }
  }
  send16(0);    // crc not computed YET
}

void replyAbout(){
  send16(sizeof(about)+5);  // length
  send8(0);                 // mode
  send8(about.componentId);      // 0x07
  send8(about.reportId);         // Get: 0xC7
  send16(about.versionBCD);       // 0xnndd
  send16(about.modelRevBCD);      // 0xnndd
  for (int i = 0;i<50;i++){
    send8(about.productName[i]);  // "Kromek D3S" (ASCII)
  }
  for (int i = 0; i<50;i++){
    send8(about.serialNumber[i]);  // "serial number" (ASCII)
  }
  send16(0);    // crc not computed YET
}
*/
