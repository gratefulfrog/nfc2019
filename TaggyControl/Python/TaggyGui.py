#!/usr/bin/python3

try:
    import tkinter as tk
except:
    import Tkinter as tk

okColor      = 'green'
nokColor     = 'red'
alertColor   = 'orange'
noAlertColor = 'gray85'
headerColor  = 'white'

rbNameVec = ['Personalize',
             'Wind',
             'Rewind',
             'Inspect',
             'Reset']


class Gui(tk.Frame):              
    def __init__(self,title,master=None):
        tk.Frame.__init__(self,master)
        self.master.title(title)
        #self.master.config(width=1000)
        self.pack(fill = tk.BOTH, expand=True)
        #self.master.attributes('-zoomed', True)
        self.rbVar = tk.StringVar()
        self.nbVar = tk.IntVar()
        self.createWidgetsFrame0()

    def createWidgetsFrame0(self):
        self.mainFrame = self.createPanelFrame(self,tk.TOP)
        self.stopGoFrame = self.createPanelFrame(self.mainFrame,tk.LEFT,False)
        self.radioButtonFrame = self.createPanelFrame(self.mainFrame,tk.LEFT,False)
        self.createGoStopFrame()
        self.createNumberEntry()
        self.createRadioButtons()
        self.createInfoDisplay(self.mainFrame,tk.LEFT)
        self.createGraphicDisplay(self)
        self.quitButton = self.createQuitButton(self)

    def createGoStopFrame(self):
        self.goButton = tk.Button(self.stopGoFrame,
                                  bg   = okColor,
                                  text = 'Go',
                                  command=self.quit)
        self.goButton.pack(side=tk.TOP,fill=tk.BOTH, expand=True)
        self.stopButton = tk.Button(self.stopGoFrame,
                                    bg   = nokColor,
                                    text = 'Stop',
                                    command=self.quit)
        self.stopButton.pack(side=tk.TOP,fill=tk.BOTH, expand=True)

    def createNumberEntry(self):
        self.row = tk.Frame(self.radioButtonFrame)
        self.row.pack(side=tk.TOP)
        self.nbLab = tk.Label(self.row,text= 'Nb Tags : ',anchor=tk.W)
        self.nbLab.pack(side=tk.LEFT)
        vcmd = (self.radioButtonFrame.register(self.isDigit),
                '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')
        self.nbEntry = tk.Entry(self.row,
                                validate = 'key',
                                validatecommand = vcmd)
        self.nbEntry.pack()

    def isDigit(self,action, index, value_if_allowed,
                prior_value, text, validation_type, trigger_type, widget_name):
        return text in'0123456789'
        
        
    def createRadioButtons(self):
        for name in rbNameVec:
            tk.Radiobutton(self.radioButtonFrame,
                           text=name,
                           #padx=10,
                           variable=self.rbVar,
                           value=name,
                           command=self.doRB,
                           anchor = tk.W,
                           indicatoron=0).pack(side = tk.TOP,fill=tk.BOTH,expand=True)
    def doRB(self):
        v = 0
        try:
            v = int(self.nbEntry.get())
        except:
            pass
        txt = self.rbVar.get()
        if txt == 'Reset':
            v =''
        print(txt,v) 
        
    def createInfoDisplay(self,frame,where):
        self.infoButton = tk.Button(frame,
                                    bg   = noAlertColor,
                                    text = 'INFO',
                                    command=self.quit)
        self.infoButton.pack(side=where,fill=tk.BOTH, expand=True)

    def createPanelFrame(self,frame,where,exp=True):
        panelFrame = tk.Frame(frame,
                              borderwidth   = 4,
                              relief        = 'groove')
                          
        panelFrame.pack(side=where,
                        fill = tk.BOTH,
                        expand=exp)
        return panelFrame

    def createGraphicDisplay(self,frame):
        self.gdButton = tk.Button(frame,
                                  bg   = noAlertColor,
                                  text = 'Graphics Display',
                                  command=self.quit)
        self.gdButton.pack(side=tk.TOP,fill=tk.BOTH, expand=True)
    
    def createQuitButton(self,frame):
        quit = tk.Button(frame,
                         bg   = noAlertColor,
                         text = 'Quit',
                         command=self.quit)
        quit.pack(side=tk.BOTTOM,fill=tk.BOTH, expand=False)
        return quit
        

if __name__ == '__main__':
    gui = Gui('Taggy GUI')
    gui.mainloop()
    

