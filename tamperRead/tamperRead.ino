
/**************************************************************************/

/**************************************************************************/
#include <Wire.h>
#include <SPI.h>
#include "Adafruit_PN532.h"

// If using the breakout with SPI, define the pins for SPI communication.
#define PN532_SCK  (2)
#define PN532_MOSI (3)
#define PN532_SS   (4)
#define PN532_MISO (5)

// If using the breakout or shield with I2C, define just the pins connected
// to the IRQ and reset lines.  Use the values below (2, 3) for the shield!
#define PN532_IRQ   (2)
#define PN532_RESET (3)  // Not connected by default on the NFC Shield

// Uncomment just _one_ line below depending on how your breakout or shield
// is connected to the Arduino:

// Use this line for a breakout with a SPI connection:
Adafruit_PN532 nfc(PN532_SCK, PN532_MISO, PN532_MOSI, PN532_SS);

// Use this line for a breakout with a hardware SPI connection.  Note that
// the PN532 SCK, MOSI, and MISO pins need to be connected to the Arduino's
// hardware SPI SCK, MOSI, and MISO pins.  On an Arduino Uno these are
// SCK = 13, MOSI = 11, MISO = 12.  The SS line can be any digital IO pin.
//Adafruit_PN532 nfc(PN532_SS);

// Or use this line for a breakout or shield with an I2C connection:
//Adafruit_PN532 nfc(PN532_IRQ, PN532_RESET);

#if defined(ARDUINO_ARCH_SAMD)
// for Zero, output on USB Serial console, remove line below if using programming port to program the Zero!
// also change #define in Adafruit_PN532.cpp library file
   #define Serial SerialUSB
#endif

void setup(void) {
  #ifndef ESP8266
    while (!Serial); // for Leonardo/Micro/Zero
  #endif
  Serial.begin(115200);
  Serial.println("Hello!");

  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.print("Didn't find PN53x board");
    while (1); // halt
  }
  
  // Got ok data, print it out!
  Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX); 
  Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC); 
  Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);
  
  // Set the max number of retry attempts to read from a card
  // This prevents us from waiting forever for a card, which is
  // the default behaviour of the PN532.
  nfc.setPassiveActivationRetries(0xFF);
  
  // configure board to read RFID tags
  nfc.SAMConfig();
  
  Serial.println("Waiting for an ISO14443A card");
}

void loop(void) {
  getUID();
  sendREQAorWUPA();
  readPage0();
  getTamperState();
  delay(1000);
}

boolean getUID(){
  boolean success;
  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
  
  // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
  // 'uid' will be populated with the UID, and uidLength will indicate
  // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, &uid[0], &uidLength);

  if (success) {
    Serial.print("UID Length: ");
    Serial.print(uidLength, DEC);
    Serial.println(" bytes");
    
    Serial.println("UID Value: ");
    for (uint8_t i=0; i < uidLength; i++) {
      Serial.print("0x");
      Serial.print(uid[i], HEX); 
      Serial.print(" ");
    }
    Serial.println();
  }
  else{
    Serial.println("Failed to get UID.");    
  }
  return success;
}
 

boolean sendREQAorWUPA(){
  uint8_t   REQA = 0x26,
            WUPA = 0x52,
            replyLength = 0;
  uint8_t  ATQA[] = {0x44, 0x00};
  uint8_t  reply[4];
  
  Serial.print("sendCommand REQA result: ");
  boolean res = nfc.sendCommandCheckAck(&REQA, 1);
  Serial.println( res ?  "True" :  "False");  
  return res;
}

boolean readPage0(){
  uint8_t buffer[100];
  Serial.println("About to read page 0!");
  
  boolean success =  nfc.ntag2xx_ReadPage (0,buffer);
  if (!success){
     success = nfc.mifareultralight_ReadPage (0, buffer);
  }
  if (success){
    Serial.println("Read Page 0, Transitioned to State Active!");
  }
  else{
    Serial.println("Failed to Read Page 0 ...");
  }
  return success;
}
    
boolean getTamperState(){
  uint8_t reply[4];
  uint8_t apdu[]= {0xAF, 0x00, 0X10, 0x4F},
    replyLength;

  
  bool inlistOK =  nfc.inListPassiveTarget();
  Serial.print("inlisted : ");
  Serial.println(inlistOK ? "True" : "False");
  

  Serial.println("About to read tamper state!");
 
  boolean res = nfc.inDataExchange(&apdu[0],4,&reply[0],&replyLength);

  Serial.print("InDataExchange returned : ");
  Serial.println( res ? "True" : "False");

  boolean success = (replyLength == 4);
  if (success){
    boolean tamperState = (reply[0] == 0x00); //TRUE if NOT TAMPERED 
    Serial.print("Tamper State = ");
    Serial.println(tamperState ? "Not tampered" :  "Tampered"); 
  }
  else{
    Serial.println("Failed to obtain tamper state ...");
    Serial.println(String("reply length: ") + String(replyLength));
  }
  Serial.print("Tamper State Reply:");
  for (uint8_t i = 0 ; i< replyLength;i++){
    Serial.print(" 0x");
    Serial.print(reply[i],HEX);
  }
  Serial.println();
  return success;
}

    
