
int dec = 0,
    gap = 5,
    w   = 100,
    h   = 50,
    y   = 300-h/2;;

void setup(){
  size(800,600);
  
}

void drawLabel(int x, int y){
      rect(x,y,w,h);
}

void drawN(int n){
  for (int i = -10; i < n-1; i++){
    drawLabel(i*(w+gap),y);
  }
}

void draw(){
  background(0);
  pushMatrix();
  translate(dec,0);
  drawN(1);
  popMatrix();
  dec+=3;
}
